#! /usr/bin/env python3

from math import sqrt, sin, cos, pi
from numpy import array, roll, dot, concatenate, broadcast_to

#### Initial conditions:
# a tiny planetary system
M = array([250., 1., .1])   #  m1, m2, m3
x = array((0., 0., 25., 0., 4., 0.))
v = array((0., 0., 0., 2.8, 0., 7.))
# excentric 2-planet system, unstable but long-lived
# x, v = array((0., 0., -10., 0., 5., 0)), array((0., 0., 0., -3., 0., 7.))

# x, v = array((0., 0., -10., 0., 3., 0)), array((0., 0., 0., -5., 0., 3.))

# this works at g=1
# binary star (equal masses) + lighter planet
# M = array((8., 8., .04))
# x = array((0., -.5, 0., .5, 10., 0.))
# v = array((1.2, 0., -1.2, 0., 0., 1.))

# M = array([1., 1., 1.])

# x = array((1., 0., cos(2 * pi / 3), sin(2 * pi / 3), cos(4 * pi / 3), sin(4 * pi / 3)))
# v = 3**-.25 * array((0.,  1., cos(pi * 7 / 6), sin(pi * 7 / 6), cos(pi * 11 / 6), sin(pi * 11 / 6)))

# x = array((.9700436, -.24308753, -.9700436, .24308753, 0., 0.))
# v = array((.466203685, .43236573, .466203685, .43236573, -2 * .466203685, -2 * .43236573))


def force(x, g=1):
    # f1 =  -g * x / sqrt((x * x).sum()) ** 3
    # y = x - [1., 0.]
    # return f1 - g * y / sqrt((y * y).sum()) ** 3

    # the version here is the fastest, of course it's specialcased to N=3

    x1 = x[:2]
    x2 = x[2:4]
    x3 = x[4:6]
    x12 = x1 - x2
    x23 = x2 - x3
    x31 = x3 - x1
    r12 = sqrt((x12 ** 2).sum())
    r23 = sqrt((x23 ** 2).sum())
    r31 = sqrt((x31 ** 2).sum())
    f12 = x12 / r12**3
    f23 = x23 / r23**3
    f31 = x31 / r31**3
    return g * concatenate((-M[1] * f12 + M[2] * f31,
                            M[0] * f12 - M[2] * f23,
                            -M[0] * f31 + M[1] * f23))

    # vectorized is easy to get wrong (it's wrong here)
    # and does not pay off in speedup for N=3

    # xy = x.reshape((3, 2))
    # xy1 = roll(xy, 1, axis=0)
    # dR = xy1 - xy   #  [r3 - r1, r1 - r2, r2 - r3]
    # r = sqrt((dR * dR).sum(axis=1))   #  r13, r12, r23
    # f = (r**-3 * dR.T).T
    # return ((roll(M, 1) * f.T).T - (roll(M, 2) * roll(f, 2).T).T).flatten() * g


def leapfrog(x, v, delta=.01):
    f = force(x)
    while True:
        x += v * delta + .5 * f * delta ** 2
        v += .5 * f * delta
        f = force(x)
        v += .5 * f * delta
        yield x, v


def energy(x, v, g=1.):
    v = v.reshape((3, 2))
    ekin = ((v**2).sum(axis=1) * M).sum() * .5

    x1 = x[:2]
    x2 = x[2:4]
    x3 = x[4:6]
    x12 = x1 - x2
    x23 = x2 - x3
    x31 = x3 - x1
    r12 = sqrt((x12**2).sum())
    r23 = sqrt((x23**2).sum())
    r31 = sqrt((x31**2).sum())
    epot = (M[0] * M[1] / r12 + M[1] * M[2] / r23 + M[0] * M[2] / r31) * (-g)
    return ekin + epot

if __name__ == '__main__':

    from sys import argv, exit

    from PyQt5.QtCore import QTimer
    from PyQt5.QtWidgets import QMainWindow, QApplication

    import pyqtgraph as pg

    pg.setConfigOption('background', '#2a2b35')
    pg.setConfigOption('foreground', 0.98)

    app = QApplication(argv)
    mw = QMainWindow()
    mw.setGeometry(100, 100, 800, 600)


    pw = pg.PlotWidget()
    pw.setAspectLocked(True)
    # pw.getViewBox().setRange(xRange=(-100, 100), yRange=(-100, 100))
    pw.addLegend()
    timer = QTimer()

    mw.keyPressEvent = (
        lambda ev:
            app.quit() if ev.text() == 'q'
            else timer.start(40) if ev.text() == ' ' and not timer.isActive()
            else timer.stop() if ev.text() == ' ' and timer.isActive()
            else ev.ignore())
    mw.setCentralWidget(pw)
    mw.setContentsMargins(5, 5, 5, 5)

    # Get rid of center of mass motion

    vcm = M @ v.reshape((3, 2)) / M.sum()
    v = v - broadcast_to(vcm, (3, 2)).flatten()

    # xnext, vnext = leapfrog(x, v)
    # track = array((x, xnext))
    # origin = array([0., 0.])
    from math import log10
    mmin = min(M)
    dotsize = [2 * log10(m/mmin) + 4 for m in M]

    pw.plot(array([x[:2]]),
        pen=None, symbol='o',
        symbolBrush=pg.mkBrush(color='y'), symbolSize=dotsize[0], pxMode=True, name=f'm1={M[0]}')
    pw.plot(array([x[2:4]]),
        pen=None, symbol='o',
        symbolBrush=pg.mkBrush(color='r'), symbolSize=dotsize[1], pxMode=True, name=f'm2={M[1]}')
    pw.plot(array([x[4:6]]),
        pen=None, symbol='o',
        symbolBrush=pg.mkBrush(color='m'), symbolSize=dotsize[2], pxMode=True, name=f'm3={M[2]}')
    # pw.plot(track, pen=pg.mkPen(width=3, color=.3, style=2))
    # pw.plot(track[-1:], symbol='o', pen=None, symbolBrush=pg.mkBrush(color='m'))


    # x, v = xnext, vnext

    steps = 0
    e0 = energy(x, v)
    print(f'initial energy: {energy(x, v)}')


    dt = .1  # milliseconds real time
    delta = dt / 4000  # in simulation time units
    # i.o.w., a unit of simulation time = 4 seconds real clock time

    pw.setTitle(f'3-body system simulation: δt = {dt} ms; 25 fps')
    pw.autoRange()
    pw.disableAutoRange()
    dataItems = pw.listDataItems()

    next_step = leapfrog(x, v, delta)

    def animate():
        global steps  # , track
        N = int(.01 // delta)  # 40 ms (real) = .01 unit of simulation time
        for _ in range(N - 1):
            next(next_step)
        x[:], v[:] = next(next_step)

        # track = concatenate((track, (xnext,)))
        dataItems[-3].setData(array([x[:2]]))
        dataItems[-2].setData(array([x[2:4]]))
        dataItems[-1].setData(array([x[4:]]))

        # if steps % 300 == 299:
        #     pw.autoRange(padding=.2)
        #     print(steps)
        steps += 1
        if steps % 250 == 0:
            print(f'{steps // 25} secs: energy deviation = {energy(x, v) / e0 - 1:.3g}')


    timer.timeout.connect(animate)
    mw.show()
    exit(app.exec())
