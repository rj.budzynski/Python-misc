#! /usr/bin/python3

# from PyQt5 import Qt
from PyQt5.QtCore import QTimer, Qt
from PyQt5.QtGui import QIntValidator
from PyQt5.QtWidgets import (QMainWindow, QApplication, QDialog, QCheckBox,
                             QDialogButtonBox, QLineEdit, QGridLayout, QLabel,
                             QMessageBox, QComboBox)
import pyqtgraph as pg
import numpy as np
import sys

pg.setConfigOption('background', '#2a2b35')
pg.setConfigOption('foreground', 0.98)


class WavePlot(object):

    wavetypes = ('nondispersive', 'schroedinger', 'schroedinger_modsquared',
                 'kleingordon', 'deepwater', 'string')
    wavetype = wavetypes[0]
    kmax = 65
    onlyOddHarmonics = False
    suppressHigherHarmonics = True
    setupWidget = None
    timer = None
    legend = None

    def __init__(self, Xmin=0., Xmax=1., npoints=300):
        self.widget = pg.PlotWidget()
        self.X = np.linspace(Xmin, Xmax, npoints)
        self.Xmin, self.Xmax = Xmin, Xmax
        self.clabel = pg.LabelItem(
            '© 2018 Robert J Budzyński', justify='right')
        self.widget.plotItem.layout.addItem(self.clabel, 4, 1,
                                            Qt.AlignLeft | Qt.AlignBaseline)

    def setup(self,
              wavetype=None,
              onlyOddHarmonics=None,
              suppressHigherHarmonics=None,
              rate=np.pi / 200,
              kmax=None,
              interval=50):
        if self.timer:
            self.timer.stop()
            del self.timer
        self.widget.clear()
        if kmax is not None:
            self.kmax = kmax
        if onlyOddHarmonics is not None:
            self.onlyOddHarmonics = onlyOddHarmonics
        if suppressHigherHarmonics is not None:
            self.suppressHigherHarmonics = suppressHigherHarmonics
        self.kValues = np.arange(1, self.kmax + 1, 2
                                 if self.onlyOddHarmonics else 1)
        if wavetype is not None:
            self.wavetype = wavetype
        if self.wavetype == 'nondispersive':
            self.update = self.update_nondispersive
            self.widget.setTitle('fala bezdyspersyjna (<i>ω = ck</i>)')
        elif self.wavetype == 'schroedinger':
            self.update = self.update_schroedinger
            self.widget.setTitle(
                'fala Schrödingera (<i>ω = αk<sup>2</sup></i>)')
        elif self.wavetype == 'schroedinger_modsquared':
            self.update = self.update_schroedinger_modsquared
            self.widget.setTitle(
                'fala Schrödingera (<i>ω = αk<sup>2</sup></i>): '
                'gęstość prawdopodobieństwa'
            )
        elif self.wavetype == 'kleingordon':
            self.update = self.update_kleingordon
            self.widget.setTitle(
                'fala Kleina-Gordona '
                '(<i>ω = sqrt(k<sup>2</sup> + m<sup>2</sup></i>))'
            )
        elif self.wavetype == 'deepwater':
            self.update = self.update_deepwater
            self.widget.setTitle(
                'fala głębokowodna (<i>ω = sqrt(gk)</i>)'
            )
        elif self.wavetype == 'string':
            self.update = self.update_string
            self.widget.setTitle(
                'fala na strunie ze sztywnością '
                '(ω = sqrt(k<sup>2</sup> + αk<sup>4</sup>))'
            )
        else:
            raise ValueError(f'nieznany typ fali: {self.wavetype}')
        self.Ys = np.array([(1 / k
                             if self.suppressHigherHarmonics else 1) * np.sin(
                                 k * self.X * np.pi) for k in self.kValues])
        Y = self.Ys.sum(axis=0)
        if self.wavetype == 'schroedinger_modsquared':
            self.Ys = .2 * self.Ys
            Y = .2 * Y
            self.widget.plot(self.X, Y ** 2, antialias=True)
        else:
            self.widget.plot(self.X, Y, antialias=True, name='Re(Ψ)')
        if self.wavetype == 'schroedinger':
            self.widget.plot(
                self.X, np.zeros(self.X.size, ), antialias=True, name='Im(Ψ)')
        self.widget.setLabels(bottom='X', left='wychylenie')
        self.widget.showGrid(x=True, y=True)
        self.widget.getViewBox().setRange(
            xRange=(self.Xmin, self.Xmax), yRange=(-Y.max(), Y.max()))
        self.dataItems = self.widget.listDataItems()
        if self.wavetype == 'schroedinger':
            self.dataItemIm = self.dataItems[1]
            self.dataItemIm.setPen('m', width=2)
            if self.legend is None:
                self.legend = pg.LegendItem(
                    offset=(self.widget.width() - 166, 30))
                self.legend.addItem(
                    self.dataItem,
                    '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Re(Ψ)')
                self.legend.addItem(
                    self.dataItemIm,
                    '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Im(Ψ)')
                self.legend.setParentItem(self.widget.getPlotItem())
            self.legend.show()
        else:
            if self.legend is not None:
                self.legend.hide()
        self.dataItem = self.dataItems[0]
        self.dataItem.setPen('y', width=2)
        self.rate = rate
        self.timer = QTimer()
        self.interval = interval
        self.timer.timeout.connect(self.update)
        self.widget.keyPressEvent = self.keyPressEvent
        self._phase = 0.

    def update_nondispersive(self):
        self._phase += self.rate
        self.dataItem.setData(
            self.X,
            np.dot(
                self.Ys.T,
                np.cos(self._phase * self.kValues)))

    def update_schroedinger(self):
        self._phase += self.rate
        Y = np.dot(self.Ys.T, np.exp(-.1j * self._phase * self.kValues ** 2))
        self.dataItem.setData(self.X, np.real(Y))
        self.dataItemIm.setData(self.X, np.imag(Y))

    def update_schroedinger_modsquared(self):
        self._phase += self.rate
        Y = np.dot(self.Ys.T, np.exp(-.1j * self._phase * self.kValues**2))
        self.dataItem.setData(self.X, np.abs(Y) ** 2)

    def update_kleingordon(self):
        self._phase += self.rate
        self.dataItem.setData(
            self.X,
            np.dot(self.Ys.T,
                   np.cos(
                       self._phase * np.sqrt(self.kValues ** 2 + 1))))

    def update_deepwater(self):
        self._phase += self.rate
        self.dataItem.setData(
            self.X,
            np.dot(self.Ys.T,
                   np.cos(self._phase * np.sqrt(5 * self.kValues))))

    def update_string(self):
        self._phase += self.rate
        ks = self.kValues
        omegas = np.sqrt(ks ** 2 + .0002 * ks ** 4)
        self.dataItem.setData(self.X, np.dot(self.Ys.T,
                                             np.cos(self._phase * omegas)))

    def _setup(self, **kwargs):
        if not self.setupWidget.kInp.hasAcceptableInput():
            QMessageBox.warning(self.widget, 'UWAGA!',
                                'błędna wartość liczby harmonik')
            return
        self.onlyOddHarmonics = self.setupWidget.oH.isChecked()
        self.suppressHigherHarmonics = self.setupWidget.hH.isChecked()
        kInpText = self.setupWidget.kInp.text()
        if kInpText:
            self.kmax = int(kInpText)
        self.wavetype = self.setupWidget.selType.currentText()
        self.setup(**kwargs)
        self.setupWidget.hide()
        # self.timer.start(self.interval)

    def setupDialog(self):
        if not self.setupWidget:
            self.setupWidget = SetupWidget()
            self.setupWidget.rejected.connect(self.setupWidget.hide)
            self.setupWidget.accepted.connect(self._setup)
            self.setupWidget.oH.setChecked(self.onlyOddHarmonics)
            self.setupWidget.hH.setChecked(self.suppressHigherHarmonics)
        self.setupWidget.selType.setCurrentIndex(
            WavePlot.wavetypes.index(self.wavetype))
        self.setupWidget.show()
        self.setupWidget.raise_()
        self.setupWidget.activateWindow()

    def keyPressEvent(self, event):
        text = event.text()
        if text == ' ':
            if self.timer.isActive():
                self.timer.stop()
            else:
                self.timer.start(self.interval)
        elif text == 'r':
            self.timer.stop()
            del self.timer
            self.widget.clear()
            self.setup(self.wavetype)
        elif text == 'c':
            nextwavetype = self.wavetypes[(
                self.wavetypes.index(self.wavetype) + 1) % len(self.wavetypes)]
            self.timer.stop()
            del self.timer
            self.widget.clear()
            self.setup(nextwavetype)
        elif text == '+':
            # self.timer.stop()
            self.rate *= 1.2
            # self.timer.start(self.interval)
        elif text == '-':
            # self.timer.stop()
            self.rate /= 1.2
            # self.timer.start(self.interval)
        elif text == 'S':
            self.setupDialog()
        else:
            event.ignore()


class SetupWidget(QDialog):
    def __init__(self, **kwargs):
        super(SetupWidget, self).__init__(**kwargs)
        self.oH = QCheckBox('tylko harmoniki nieparzyste?')
        self.hH = QCheckBox('tłumić wyższe harmoniki?')
        self.kInp = QLineEdit()
        self.kInp.setMaxLength(3)
        self.kInp.setPlaceholderText(str(plot.kmax))
        self.kInp.setValidator(QIntValidator(1, 999))
        self.selType = QComboBox(self)
        self.selType.addItems(WavePlot.wavetypes)
        self.selType.setInsertPolicy(QComboBox.NoInsert)

        self.bBox = QDialogButtonBox(QDialogButtonBox.Ok
                                     | QDialogButtonBox.Cancel)
        layout = QGridLayout()
        self.setLayout(layout)
        layout.addWidget(self.oH, 0, 0)
        layout.addWidget(self.hH, 1, 0)
        layout.addWidget(QLabel('liczba harmonik: '), 2, 0)
        layout.addWidget(self.kInp, 2, 1)
        layout.addWidget(QLabel('wybierz typ fali:'), 3, 0)
        layout.addWidget(self.selType, 4, 0)
        layout.addWidget(self.bBox, 5, 0)
        self.rejected, self.accepted = self.bBox.rejected, self.bBox.accepted


if __name__ == '__main__':
    app = QApplication(sys.argv)
    w = QMainWindow()
    w.setGeometry(100, 100, 800, 600)
    w.setWindowTitle('Ruch falowy we wnęce z dypersją - animacja')
    w.keyPressEvent = (
        lambda ev: app.quit() if ev.text() == 'q' else ev.ignore())
    plot = WavePlot()
    plot.setup(kmax=100)
    w.setCentralWidget(plot.widget)
    w.statusBar().showMessage(
        "pyqtgraph w działaniu... naciśnij 'q' aby zakończyć; "
        "Spację aby zatrzymać / wznowić; "
        "'r' aby zacząć od początku; "
        "'c' aby zmienić typ fali; "
        "'+' przyspiesza, '-' spowalnia; "
        "'S' dla zmiany ustawień")
    w.setContentsMargins(7, 7, 7, 7)
    w.show()
    # sW = SetupWidget()
    # sW.show()
    sys.exit(app.exec_())
