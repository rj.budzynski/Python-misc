# Various examples, demos & API exercises in Python

The gravitational dynamics simulations are done for 2D only. 
It is not hard to extend the dynamics to 3D, but I wasn't 
ready to struggle with 3D visualization at that time.

## `wave_animation.py`

Demonstrates the time evolution of different one-dimensional waveforms
under fixed-ends boundary conditions and with different dispersion
relations. Configurable via a dialog. Uses: PyQtGraph, PyQt5.

## `polygonsmoothing.py`

The idea comes from [this paper](https://doi.org/10.1137/090746707).

## `dynamics.py`, `initial_conditions.py`

Simple module for numerical simulation of Newtonian dynamics (celestial dynamics).
And a small collection of interesting initial conditions.

## `dynamics0.py`

A rather messy simulation of a 3-body problem, with GUI. Uses: PyQtGraph, PyQt5.

## `pgdynamics.py`

Newtonian many (few) -body problem done a bit better. Uses: PyQtGraph, PyQt5.
Invoke with an integer argument (currently 0:6) to use one of the pre-fab 
initial conditions. Click and drag to pan, mouse wheel to scroll.

## `browngas.py`

A "gas" of Brownian particles, ie. moving independently by random walk.
You can choose the dimensionality, number of particles, and the speed &mdash;
governed by the update interval (in milliseconds). Calling it a *gas* is 
somewhat bogus, since the particles don't collide or intereact in any way.

## `qtchart/`

Some experiments with the QtCharts library. Pretty much discarded.

## `pygame/videonoise-pygame.py`

Just throwing random pixels at the screen - which pygame is pretty fast at.

## `pygame/ising.py`

The fastest I could make a 2d Ising model simulation work in Python.
Requires the `bitarray` module (search PyPI).
Still more than 20x slower than the equivalent in Javascipt (V8).
