#!/usr/bin/env python3

from PyQt5.QtWidgets import QMainWindow, QApplication
from PyQt5.QtCore import QTimer, Qt

import pyqtgraph as pg

from numpy import array
# from numpy.random import rand
from sys import argv, exit

from dynamics import NewtonianDynamics

pg.setConfigOption('background', '#2a2b35')
pg.setConfigOption('foreground', 0.98)


class NewtonianDynamicsGui(pg.PlotWidget):

    _symbolBrush = pg.mkBrush(color='y')
    model = NewtonianDynamics(dim=2)

    def setup(self, X, V, δ=.01, fps=25):

        assert X.shape == V.shape

        self.nParticles, self.dim = X.shape
        self.interval = 1000 / fps
        self.delta = δ
        self.setAspectLocked(True)
        self.disableAutoRange()
        self.addLegend()
        self.timer = QTimer()

        self.X0, self.V0 = X.copy(), V.copy()

        self.step = self.model.step(X, V, δ)
        self.stepsPerFrame = int(self.interval / 4000 // δ)

        from math import log10
        mmin = min(self.model.masses)
        dotsize = [2 * log10(m/mmin) + 4 for m in self.model.masses]

        for k, xy in enumerate(self.X0):
            self.plot(array([xy]), symbol='o', pen=None,  # symbolSize=6,
                      pxMode=True,
                      symbolBrush=pg.mkBrush(color=k),
                      symbolSize=dotsize[k],
                      name=f'm{k}={self.model.masses[k]}')

        # self.autoRange()
        self._ticks = 0
        self.timeLabel = pg.LabelItem('sim time: 0.', justify='left')
        self.plotItem.layout.addItem(self.timeLabel, 4, 1, 
                                     Qt.AlignLeft | Qt.AlignBaseline)

        self.timer.timeout.connect(self.update)

    def toggleRunning(self):
        if self.timer.isActive():
            self.timer.stop()
            self.parent().statusBar().showMessage('Paused.')
        else:
            self.timer.start(self.interval)
            self.parent().statusBar().showMessage('Running.')

    # def setFps(self, fps):
    #     if self.timer.isActive():
    #         self.timer.stop()
    #         self.interval = 1000 / fps
    #         self.timer.start()
    #     else:
    #         self.interval = 1000 / fps

    def keyPressEvent(self, ev):
        text = ev.text()
        if text == ' ':
            self.toggleRunning()
        else:
            ev.ignore()

    def symbolBrush(self):
        return self._symbolBrush

    def update(self):

        if not self._ticks:
            self.e0 = self.model.energy(self.X0, self.V0)
            print(f'initial energy: {self.e0}')
        for _ in range(self.stepsPerFrame - 1):
            next(self.step)
        Xnext, Vnext = next(self.step)
        # print(Xnext)
        for i, d in enumerate(self.listDataItems()[-self.nParticles:]):
            d.setData(array([Xnext[i]]))
        self._ticks += 1
        self.timeLabel.setText(f'sim time: {self._ticks * self.delta:.4f}')
        if self._ticks % (10000 / self.interval) == 0:
            energy_rel_dev = self.model.energy(Xnext, Vnext) / self.e0 - 1
            print(f'{self._ticks / 1000 * self.interval} secs: '
                  'energy deviation = '
                  f'{energy_rel_dev:e}')
            if abs(energy_rel_dev) > .1:
                self.timer.stop()
                self.parent().statusBar().showMessage(
                    'STOP. Catastrophic breakdown!'
                )


def main(argv):

    from initial_conditions import __all__ as allIC

    app = QApplication(argv)

    try:
        which = int(argv[1])
        icond = allIC[which]
    except (IndexError, ValueError, KeyError):
        icond = allIC[0]

    masses, X, V = icond.masses, icond.X, icond.V

    # nParticles, height, width = 3, 1.5, 1.5  # nParticles yet unused

    # for a in argv[1:]:
    #     if a.startswith('n='):
    #         nParticles = int(a.split('=')[1])
    #     elif a.startswith('width='):
    #         width = float(a.split('=')[1])
    #     elif a.startswith('height='):
    #         height = float(a.split('=')[1])

    gui = NewtonianDynamicsGui()
    gui.model.nparticles = masses.size
    gui.model.setMasses(masses)
    gui.model.force = gui.model.forceTwobodyCoulomb
    δ = .00005  # in units of simulations time: 1 s.t.u. = 4 s
    gui.setup(X, V, δ, fps=25)
    gui.autoRange()
    gui.disableAutoRange()
    gui.setTitle(f'N-body system simulation: δt = {δ * 4000:g} ms; '
                 f'{1000/gui.interval} fps')

    mw = QMainWindow()
    mw.setGeometry(100, 100, 800, 600)
    mw.keyPressEvent = (
        lambda ev:
            app.quit() if ev.text() == 'q'
            else ev.ignore())
    mw.setCentralWidget(gui)
    mw.statusBar().showMessage('Paused.')
    mw.setContentsMargins(5, 5, 5, 5)

    mw.show()
    exit(app.exec())


if __name__ == '__main__':

    main(argv)
