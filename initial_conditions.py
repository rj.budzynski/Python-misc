# NewtonianDynamicsData
'''This module provides a collection of initial conditions for celestial
mechanics simulations.
'''

from numpy import array


__all__ = []


class InitialConditions(object):
    def __init__(self, **kwargs):
        for key, val in kwargs.items():
            self.__setattr__(key, array(val))
        __all__.append(self)


# simple fast-rotating symmetric twobody, for testing
o0 = InitialConditions(
    X=((0., -.5), (0., .5)), V=((4., 0.), (-4, 0.)), masses=(32, 32))

# a binary system + planet + satellite
o1 = InitialConditions(
    masses=(10., 10., .04, .004),
    X=((0., -.25), (0., .25), (5., 0.), (5.2, 0)),
    V=((2.6, 0.), (-2.6, 0.), (0., 1.9), (0., 1.5)))

# a tiny planetary system
o2 = InitialConditions(
    masses=(250., 1., .1),   # m1, m2, m3
    X=((0., 0.), (25., 0.), (4., 0.)),
    V=((0., 0.), (0., 2.8), (0., 7.)))

# three equal masses running around a figure-eight
o3 = InitialConditions(
    X=((.9700436, -.24308753), (-.9700436, .24308753), (0., 0.)),
    V=((.466203685, .43236573), (.466203685, .43236573), (-2 * .466203685,
                                                          -2 * .43236573)),
    masses=(1., 1., 1.))

# four masses in a symmetric configuration, oscillating
o4 = InitialConditions(
    X=((1., 0.), (0., 1.), (-1., 0.), (0., -1)),
    V=((0., .75), (-.75, 0.), (0., -.75), (.75, 0.)),
    masses=(1., 1., 1., 1.))

# four masses in a symmetric configuration, a rotating square
o5 = InitialConditions(
    X=((1., 0.), (0., 1.), (-1., 0.), (0., -1)),
    V=((0., 1.), (-1., 0.), (0., -1.), (1., 0.)),
    masses=(1., 1., 1., 1.))
