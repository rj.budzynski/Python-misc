#! /usr/bin/python3

from numpy import indices, array, sin, cos, pi, uint32, uint8
from numba import vectorize, jit


WIDTH, HEIGHT, N = 600, 600, 6
BGCOLOR = array((0xff, 0x88, 0xff)).astype(float)


@vectorize
def imgfilter(x):
    return 1 - x % 1 if (x // 1) % 2 else x % 1


def init(width=WIDTH, height=HEIGHT, n=N):

    X, Y, K = indices((width, height, n))
    return (X * sin(pi * K / n) - Y * cos(pi * K / n)) / 5


@jit(parallel=True, cache=True)
def render(W, t):

    n = W.shape[-1]
    alpha = sin(W - t/1000).sum(axis=2)
    alpha = ((alpha + n) / 2)
    alpha = imgfilter(alpha)
    img = (BGCOLOR[0] * alpha).astype(uint32) << 16 | (BGCOLOR[1] * alpha).astype(uint32) << 8 | (BGCOLOR[2] * alpha).astype(uint32)
    #img = multiply.outer(alpha, BGCOLOR).astype(uint8)
    return img


if __name__ == "__main__":

    from sys import argv
    args = argv[1:]
    n = N
    if args:
        try:
            n = int(args.pop(0))
            if n < 1: raise ValueError
        except ValueError:
            exit('ERROR: number of waves must be an int > 0')
    import pygame
    pygame.init()
    screen = pygame.display.set_mode((WIDTH, HEIGHT))
    pygame.display.set_caption(f'Quasicrystal: n={n}')
    clock = pygame.time.Clock()
    font = pygame.font.SysFont('Courier New, courier, monospace', 24, bold=True)
    text = font.render('fps: ?', True, (255, 255, 0))
    t = dt = 0
    running = True
    W = init(n=n)
    while running:
        dt += clock.tick(30)
        t += clock.get_time()
        pygame.surfarray.pixels2d(screen)[:, :] = render(W, t)
        if dt > 1e3:
            text = font.render(f'fps: {clock.get_fps():.1f}', True, (255, 255, 0))
            dt = 0
        screen.blit(text, (10, 10))
        pygame.display.flip()
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
                break
