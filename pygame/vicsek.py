#! /usr/bin/python3
"""
A simple-minded implementation of the Vicsek model in finite-sized 2D with
periodic boundary conditions.

Let the interaction radius be the unit of length, and the size of the space
domain W x H: integers. Let the time step be the unit of time. The remaining
parameters are N - the number of particles, V - the constant speed, and the
noise - not only the amplitude, but possibly different shapes.

 - Positions are described by an ndarray of shape 2, N
 - angles are shape N.
"""

import numpy as np

TWO_PI = np.pi * 2
W, H = 160, 90
dimensions = np.array((W, H), dtype='int')
v = .2
right, left, up, down = (
    np.array((1, 0)), np.array((-1, 0)),
    np.array((0, 1)), np.array((0, -1)))


def initialize(dimensions, N):
    """
    You can play with this, mainly for visual effect.
    """
    # positions = ((np.random.random((2, N)).T / 2 + .25) * dimensions).T
    radii = np.sqrt(1. - np.random.random(N)) * dimensions.min() * .25
    phis = np.random.random(N) * TWO_PI
    positions = (dimensions / 2 + np.array(radii *
                                           (np.cos(phis), np.sin(phis))).T).T
    angles = np.random.random(N) * TWO_PI
    return positions, angles


def move(positions, angles):

    positions[...] = positions + v * np.array((np.cos(angles), np.sin(angles)))
    positions[...] = (positions.T % dimensions).T
    return positions


def align2(positions, angles):
    """
    A different implementation: no cells, account for images under periodic b.c.'s.
    """
    new_angles = np.empty(angles.shape)
    for i, xy in enumerate(positions.T):
        x, y = xy
        X, Y = positions
        nearby = np.hypot(x - X, y - Y) < 1
        # include images here
        if x < 1 or y < 1 or x > W - 1 or y > H - 1:
            nearby |= np.hypot(x - X + W, y - Y) < 1
            nearby |= np.hypot(x - X, y - Y + H) < 1
            nearby |= np.hypot(x - X - W, y - Y) < 1
            nearby |= np.hypot(x - X, y - Y - H) < 1
            nearby |= np.hypot(x - X + W, y - Y + H) < 1
            nearby |= np.hypot(x - X - W, y - Y - H) < 1
            nearby |= np.hypot(x - X + W, y - Y - H) < 1
            nearby |= np.hypot(x - X - W, y - Y + H) < 1
        nearby_angles = angles[nearby]
        avg_sin, avg_cos = (np.sin(nearby_angles).mean(),
                            np.cos(nearby_angles).mean())
        new_angles[i] = np.arctan2(avg_sin, avg_cos)
    new_angles[...] += noise(angles.size)
    angles[...] = new_angles % (TWO_PI)
    return angles


def order_parameter(angles):

    return np.hypot(np.sin(angles).mean(), np.cos(angles).mean())


def noise(N):

    return (np.random.random((N, )) - .5) * TWO_PI * .1


def update(positions, angles):

    move(positions, angles)
    align2(positions, angles)
    return positions, angles


def main():
    """
    The visualization.
    """
    import pygame

    BLACK = pygame.Color(0, 0, 0)
    TRANSLUCENT = pygame.Color(0, 0, 0, 24)
    GOLD = pygame.Color(255, 215, 0)
    RADIUS = 4
    display_factor = 7
    width, height = display_factor * W, display_factor * H
    N = 666
    paused = True

    def paint(screen, positions, color=pygame.Color(0, 0, 0)
              ):

        for pos, phi in zip(positions.T, angles):
            pos = np.round(pos * display_factor).astype('int')
            color.hsva = 360 * phi / TWO_PI, 100, 100, 100
            pygame.draw.circle(screen, color, pos, RADIUS)

    positions, angles = initialize(dimensions, N)
    order_value = order_parameter(angles)
    pygame.init()
    pygame.display.set_caption(f'Vicsek model: {N} particles')
    screen = pygame.display.set_mode((width, height))
    eraser = pygame.Surface(screen.get_size(), flags=pygame.SRCALPHA)
    eraser.fill(TRANSLUCENT)
    font = pygame.font.SysFont(
        'Courier New, courier, monospace', 16, bold=True)
    clock = pygame.time.Clock()
    pygame.time.set_timer(pygame.USEREVENT, 1000)
    screen.fill(BLACK)
    paint(screen, positions)
    pygame.display.flip()

    while True:
        if paused:
            clock.tick(1)
        else:
            clock.tick(20)
            screen.blit(eraser, (0, 0))
            paint(screen, positions)
            text1 = font.render(f'fps: {clock.get_fps():.0f}', True, GOLD)
            text2 = font.render(f'order param.: {order_value:.3f}', True, GOLD)
            screen.blit(text1, (10, 10))
            screen.blit(text2, (10, 30))
            update(positions, angles)

            pygame.display.flip()
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                exit()
            elif event.type == pygame.USEREVENT:
                order_value = order_parameter(angles)
            elif event.type == pygame.MOUSEBUTTONUP and event.button == 1:
                paused = not paused
                if not paused:
                    clock.tick()


if __name__ == '__main__':

    main()
