#! /usr/bin/python3

from random import random, getrandbits, randrange
from math import log
from numpy import array
from bitarray import bitarray
from bitarray.util import int2ba
import pygame

COLS, ROWS = 512, 512

class Ising:

    def __init__(self, cols, rows, J=1, h=0):

        self.cols, self.rows = cols, rows
        self.size = cols * rows
        self.J, self.h = J, h
        self.lattice = bitarray(self.size)
        self.lattice[:] = 0


    def align(self, sign):

        self.lattice[:] = 1 if sign > 0 else 0
        return self


    def randomize(self):

        self.lattice[:] = int2ba(getrandbits(self.size), length=self.size)
        return self


    @property
    def magnetization(self):

        return 2 * sum(self.lattice) / self.size - 1


    @property
    def state_array(self):

        arr = array(list(self.lattice), dtype='uint32').reshape((self.cols, self.rows))
        return arr


    def sum_of_neighbors(self, k):

        size, cols, rows = self.size, self.cols, self.rows
        bits = self.lattice
        x, y = k % cols, k // rows
        return (
            bits[(k + 1) if (x + 1 != cols) else (k - cols + 1)] +
            bits[(k - 1) if x else (k + cols - 1)] +
            bits[(k - cols) % size] +
            bits[(k + cols) % size]
            )


    def metropolis_sweep(self):

        size, cols, rows = self.size, self.cols, self.rows
        bits = self.lattice
        for k in range(size):
            r = 1 - random() # this can never be 0
            s = 2 * bits[k] - 1
            x, y = k % cols, k // rows
            de = (
            bits[(k + 1) if (x + 1 != cols) else (k - cols + 1)] +
            bits[(k - 1) if x else (k + cols - 1)] +
            bits[(k - cols) % size] +
            bits[(k + cols) % size]
            )
            de = de * 2 - 4
            de *= 2 * self.J * s
            de += 2 * self.h * s
            # de <= 0 quite often, removing the need to compute a log()
            if de <= 0 or de < -log(r):
                self.lattice[k] ^= 1


    def glauber(self):

        size = self.size
        cols, rows = self.cols, self.rows
        n = size + 1
        bits = self.lattice
        while n := n - 1:
            k = randrange(size)
            s = bits[k] * 2 - 1
            x, y = k % cols, k // rows
            de = (
            bits[(k + 1) if (x + 1 != cols) else (k - cols + 1)] +
            bits[(k - 1) if x else (k + cols - 1)] +
            bits[(k - cols) % size] +
            bits[(k + cols) % size]
            )
            de = de * 2 - 4
            de *= 2 * self.J * s
            de += 2 * self.h * s
            if de < log(1 / (1 - random()) - 1):
                self.lattice[k] ^= 1


    def heatbath(self):

        size = self.size
        cols, rows = self.cols, self.rows
        n = size + 1
        bits = self.lattice
        while n := n - 1:
            k = randrange(size)
            x, y = k % cols, k // rows
            de = (
            bits[(k + 1) if (x + 1 != cols) else (k - cols + 1)] +
            bits[(k - 1) if x else (k + cols - 1)] +
            bits[(k - cols) % size] +
            bits[(k + cols) % size]
            )
            de = de * 2 - 4
            de *= 2 * self.J
            de += 2 * self.h
            if -de < log(1 / (1 - random()) - 1):
                bits[k] = 1
            else:
                bits[k] = 0


if __name__ == "__main__":

    pygame.init()
    screen = pygame.display.set_mode((COLS, ROWS))
    pygame.display.set_caption('Ising model in 2D')
    clock = pygame.time.Clock()
    clock.tick()
    running = True
    ising = Ising(COLS, ROWS, log(1 + 2 ** 0.5) / 2, .001).randomize()
    count = 0

    while running:
        pygame.surfarray.pixels2d(screen)[:, :] = (1 - ising.state_array) * 0xffffff
        pygame.display.flip()
        pygame.display.set_caption(
            f'Ising model in 2D: {clock.tick()} ms/frame; M: {ising.magnetization:.4f}'
        )
        #ising.metropolis_sweep()
        ising.glauber()
        #ising.heatbath()
        count += 1
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
                break





