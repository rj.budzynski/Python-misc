## pygame demos

The `*-jit.py` require `numba`.

The non-jit `ising.py` uses `bitarray` (find it on PyPI), is the fastest one can
get without applying `numba.jit`, but is still quite slow. `ising-jit.py` is
on par with a javascript implementation running on a current browser.
