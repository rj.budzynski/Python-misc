#! /usr/bin/python3

from numpy import array, indices, zeros, sin, cos, pi, vectorize
import pygame

WIDTH, HEIGHT, N = 600, 600, 7
BGCOLOR = array((0xff, 0x66, 0xff))


@vectorize
def imgfilter(x):
    return 1 - x % 1 if (x // 1) % 2 else x % 1


class Qcrystal:

    def __init__(self, width=WIDTH, height=HEIGHT, n=N):

        self.width = width
        self.height = height
        self.n = n
        X, Y, K = indices((width, height, n))
        self.W = (X * sin(pi * K / N) - Y * cos(pi * K / N)) / 5


    def render(self, t):

        W, N = self.W, self.n
        alpha = sin(W - t/1000).sum(axis=2)
        alpha = ((alpha + N) / 2)
        alpha = imgfilter(alpha)
        img = (BGCOLOR[0] * alpha).astype('uint32') << 16 | (BGCOLOR[1] * alpha).astype('uint32') << 8 | (BGCOLOR[2] * alpha).astype('uint32')
        return img


if __name__ == "__main__":

    pygame.init()
    screen = pygame.display.set_mode((WIDTH, HEIGHT))
    pygame.display.set_caption('Quasicrystal')
    clock = pygame.time.Clock()
    t = clock.tick(25)
    running = True
    qcr = Qcrystal(WIDTH, HEIGHT, N)
    while running:
        t += clock.tick(25)
        pygame.surfarray.pixels2d(screen)[:, :] = qcr.render(t)
        pygame.display.flip()
        #print(clock.get_fps())
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
                break
