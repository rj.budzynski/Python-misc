#! /usr/bin/python3

from math import log
from numpy import array, zeros, empty
from numpy.random import randint, random
import pygame
import numba

COLS, ROWS = 640, 640
BGCOLOR = 0xff0fff


def init(cols, rows):

    return zeros((cols, rows), dtype='bool')


def align(lattice, sign):

    lattice[:] = 1 if sign > 0 else 0
    return lattice


def randomize(lattice):

    lattice[:] = randint(0, 2, size=lattice.shape, dtype='bool')
    return lattice


def get_magnetization(lattice):

    return 2 * lattice.sum() / lattice.size - 1


@numba.jit
def sum_of_neighbors(lattice, x, y):

    cols, rows = lattice.shape
    return (
        lattice[(x + 1) % cols, y]
        + lattice[x - 1, y]
        + lattice[x, (y + 1) % rows]
        + lattice[x, y - 1]
    )


@numba.jit
def metropolis(lattice, J=1, h=0, N=None):

    size = lattice.size
    if N is None:
        N = size
    cols, rows = lattice.shape
    N += 1
    while N := N - 1:
        r = 1 - random()  # this can never be 0
        k = randint(size)
        x, y = k % cols, k // rows
        s = lattice[x, y] * 2 - 1
        de = sum_of_neighbors(lattice, x, y)
        de = de * 2 - 4
        de *= 2 * J * s
        de += 2 * h * s
        # de <= 0 quite often, removing the need to compute a log()
        if de <= 0 or de < -log(r):
            lattice[x, y] ^= 1


@numba.jit
def metropolis_sweep(lattice, J=1, h=0):

    size = lattice.size
    cols, rows = lattice.shape
    # R = 1 - random((cols, rows))
    for y in range(rows):
        for x in range(cols):
            r = 1 - random()  # this can never be 0
            s = lattice[x, y] * 2 - 1
            de = sum_of_neighbors(lattice, x, y)
            de = de * 2 - 4
            de *= 2 * J * s
            de += 2 * h * s
            # de <= 0 quite often, removing the need to compute a log()
            if de <= 0 or de < -log(r):
                lattice[x, y] ^= 1


@numba.jit
def glauber(lattice, J=1, h=0, N=None):

    if N is None:
        N = lattice.size
    size = lattice.size
    cols, rows = lattice.shape
    N += 1
    while N := N - 1:
        k = randint(size)
        x, y = k % cols, k // rows
        s = lattice[x, y] * 2 - 1
        de = sum_of_neighbors(lattice, x, y)
        de = de * 2 - 4
        de *= 2 * J * s
        de += 2 * h * s
        if de < log(1 / (1 - random()) - 1):
            lattice[x, y] ^= 1


@numba.jit
def checkerboard_sweep(lattice, J=1, h=0):

    cols, rows = lattice.shape
    for i in 0, 1:
        for y in range(rows):
            for x in range(y % 2 ^ i, cols, 2):
                s = lattice[x, y] * 2 - 1
                de = sum_of_neighbors(lattice, x, y)
                de = de * 2 - 4
                de *= 2 * J * s
                de += 2 * h * s
                r = 1 - random()
                if de <= 0 or de < -log(r):
                    lattice[x, y] ^= 1


if __name__ == "__main__":

    J, h = log(1 + 2 ** 0.5) / 2, .0001
    pygame.init()
    screen = pygame.display.set_mode((COLS, ROWS))
    pygame.display.set_caption('Ising model in 2D')
    font = pygame.font.SysFont(
        'Courier New, courier, monospace', 24, bold=True)
    text = font.render('fps: ?', True, (255, 255, 0))
    clock = pygame.time.Clock()
    dt = 0
    running = True
    lattice = randomize(init(COLS, ROWS))
    
    while running:
        dt += clock.tick()
        pygame.surfarray.pixels2d(screen)[:, :] = (1 - lattice) * BGCOLOR
        if dt > 1e3:
            text = font.render(
                f'fps: {clock.get_fps():.1f}; M: {100.*get_magnetization(lattice):.1f}%',
                True, (255, 255, 0)
            )
            dt = 0
        screen.blit(text, (10, 10))
        pygame.display.flip()
        # Choose an update algorithm here
        metropolis(lattice, J, h)
        # metropolis_sweep(lattice, J, h)
        # checkerboard_sweep(lattice, J, h)
        # glauber(lattice, J, h)
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
                break
