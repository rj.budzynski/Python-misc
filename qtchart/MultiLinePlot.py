#! /usr/bin/python3

from PyQt5.QtChart import QChart, QChartView, QLineSeries
from PyQt5.Qt import QPointF, QPainter, QPen, QColor
from PyQt5.QtWidgets import QApplication, QMainWindow
from numpy import array, min, max, linspace, sin, cos, pi
from sys import argv, exit

from LinePlot import LinePlot


class MultiLinePlot(LinePlot):

    def setPlotData(self, X, Y, index=0):
        series = self.series()[index]
        series.replace(
            [QPointF(x, y) for (x, y) in array([X, Y]).T])

    def setLineWidth(self, width, index=0):
        series = self.series()[index]
        pen = series.pen()
        pen.setWidth(width)
        series.setPen(pen)

    def addPlot(self, X, Y, lineWidth=None, lineColor=None):
        index = len(self.series())
        self.addSeries(QLineSeries())
        self.setPlotData(X, Y, index)
        minX, maxX = self.axisX().min(), self.axisX().max()
        minY, maxY = self.axisY().min(), self.axisY().max()
        minX, maxX = min((minX, min(X))), max((maxX, max(X)))
        minY, maxY = min((minY, min(Y))), max((maxY, max(Y)))
        self.setXrange(minX, maxX)
        self.setYrange(minY, maxY)
        self.series()[index].attachAxis(self.axisX())
        self.series()[index].attachAxis(self.axisY())
        if lineWidth is not None:
            self.setLineWidth(lineWidth, index)
        if lineColor is not None:
            self.series()[index].setColor(QColor(lineColor))

    def plot(self, *args, lineWidth=None, xGrid=None, yGrid=None, **kwargs):
        minX, maxX, minY, maxY = 0., 0., 0., 0.
        args = list(args)
        index = 0
        while args:
            X, Y = args.pop(0), args.pop(0)
            self.addSeries(QLineSeries())
            self.setPlotData(X, Y, index)
            index += 1
            minX, maxX = min((minX, min(X))), max((maxX, max(X)))
            minY, maxY = min((minY, min(Y))), max((maxY, max(Y)))
        self.createDefaultAxes()
        self.setXrange(minX, maxX)
        self.setYrange(minY, maxY)
        self.applyDefaultStyle()
        if lineWidth is not None:
            for ix in range(index):
                self.setLineWidth(lineWidth, ix)
        if xGrid is not None:
            self.axisX().setGridLineVisible(bool(xGrid))
        if yGrid is not None:
            self.axisY().setGridLineVisible(bool(yGrid))
        self.chartView = QChartView(self)
        self.chartView.setRenderHint(QPainter.Antialiasing)


if __name__ == '__main__':

    X = linspace(0., 4., 99)
    plotArgs = [X, sin(pi * X), X, cos(pi * X), X, sin(pi * X) + cos(pi * X)]
    app = QApplication(argv)
    w = QMainWindow()
    mlp = MultiLinePlot()
    mlp.plot(*plotArgs, lineWidth=3)
    mlp.addPlot(X, 4 * sin(pi * X / 2), lineWidth=6, lineColor='#aaa')
    # mlp.setYrange(-3., 3.)
    mlp.setXtitle('<b><i>X</i></b>')
    mlp.setYtitle('<b><i>Y</i></b>')
    mlp.setTitle('<b style="font-size:16pt;">Trigonometric Functions</b>')
    # w = lp.chartView
    w.setWindowTitle('a MultiLinePlot')
    w.setCentralWidget(mlp.chartView)
    w.keyPressEvent = (
        lambda ev: app.quit() if ev.text() == 'q' else ev.ignore())
    w.statusBar().showMessage('© 2018 Robert J Budzyński')
    w.resize(800, 600)
    w.show()
    exit(app.exec())
