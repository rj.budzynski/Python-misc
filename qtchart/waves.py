#! /usr/bin/python3

from PyQt5.QtWidgets import QApplication, QMainWindow, QDialog, QCheckBox, \
    QMessageBox, QLineEdit, QLabel, QComboBox, QDialogButtonBox, QGridLayout
from PyQt5.QtChart import QChart, QChartView, QLineSeries
from PyQt5.Qt import QPainter, QColor, QPointF, QTimer, QIntValidator
import sys
import numpy as np


class WavePlot(QChart):

    wavetypes = ('nondispersive', 'schroedinger', 'kleingordon', 'deepwater',
                 'string')
    wavetype = wavetypes[0]
    kmax = 65
    onlyOddHarmonics = False
    suppressHigherHarmonics = True
    setupWidget = None
    timer = None

    def __init__(self, Xmin=0., Xmax=1., npoints=300, **kwargs):
        self.X = np.linspace(Xmin, Xmax, npoints)
        self.Xmin, self.Xmax = Xmin, Xmax
        super().__init__(**kwargs)

    def setup(self,
              wavetype=None,
              onlyOddHarmonics=None,
              suppressHigherHarmonics=None,
              rate=np.pi / 200,
              kmax=None,
              interval=50):
        if self.timer:
            self.timer.stop()
            del self.timer
        # self.clear()
        if kmax is not None:
            self.kmax = kmax
        if onlyOddHarmonics is not None:
            self.onlyOddHarmonics = onlyOddHarmonics
        if suppressHigherHarmonics is not None:
            self.suppressHigherHarmonics = suppressHigherHarmonics
        self.kValues = np.arange(1, self.kmax + 1, 2
                                 if self.onlyOddHarmonics else 1)
        if wavetype is not None:
            self.wavetype = wavetype
        if self.wavetype == 'nondispersive':
            self.update = self.update_nondispersive
            self.setTitle('fala bezdyspersyjna (<i>ω = ck</i>)')
        elif self.wavetype == 'schroedinger':
            self.update = self.update_schroedinger
            self.setTitle(
                'fala Schrödingera (<i>ω = αk<sup>2</sup></i>)')
        elif self.wavetype == 'kleingordon':
            self.update = self.update_kleingordon
            self.setTitle(
                'fala Kleina-Gordona '
                '(<i>ω = sqrt(k<sup>2</sup> + m<sup>2</sup></i>))'
            )
        elif self.wavetype == 'deepwater':
            self.update = self.update_deepwater
            self.setTitle('fala głębokowodna (<i>ω = sqrt(gk)</i>)')
        elif self.wavetype == 'string':
            self.update = self.update_string
            self.setTitle(
                'fala na strunie ze sztywnością '
                '(ω = sqrt(k<sup>2</sup> + αk<sup>4</sup>))'
            )
        else:
            raise ValueError(f'nieznany typ fali: {self.wavetype}')
        self.Ys = np.array([(1 / k
                             if self.suppressHigherHarmonics else 1) * np.sin(
                                 k * self.X * np.pi) for k in self.kValues])
        Y = self.Ys.sum(axis=0)
        if self.series():
            self.removeAllSeries()
        series = QLineSeries()
        self.addSeries(series)
        self.setData(self.X, Y)
        for ax in self.axes():
            self.removeAxis(ax)
        self.createDefaultAxes()
        maxY = max(abs(Y.min()), abs(Y.max()))
        self.axisY().setRange(-1.4 * maxY, 1.4 * maxY)
        self.setDropShadowEnabled(True)
        self.axisX().setTitleText('X')
        self.axisY().setTitleText('wychylenie')
        self.legend().hide()
        self.setTheme(QChart.ChartThemeDark)
        series.setColor(QColor('yellow'))
        self.rate = rate
        self.timer = QTimer()
        self.interval = interval
        self.timer.timeout.connect(self.update)
        self._phase = 0.

    def setData(self, X, Y):
        self.series()[0].replace(
            [QPointF(x, y) for (x, y) in np.array([X, Y]).T])

    def update_nondispersive(self):
        self._phase += self.rate
        self.setData(self.X,
                     np.dot(self.Ys.T, np.cos(self._phase * self.kValues)))

    def update_schroedinger(self):
        self._phase += self.rate
        self.setData(
            self.X,
            np.dot(
                self.Ys.T,
                np.cos(.1 * self._phase * self.kValues ** 2)))

    def update_kleingordon(self):
        self._phase += self.rate
        self.setData(
            self.X,
            np.dot(self.Ys.T,
                   np.cos(
                       self._phase * np.sqrt(self.kValues ** 2 + 1))))

    def update_deepwater(self):
        self._phase += self.rate
        self.setData(
            self.X,
            np.dot(self.Ys.T,
                   np.cos(self._phase * np.sqrt(5 * self.kValues))))

    def update_string(self):
        self._phase += self.rate
        ks = self.kValues
        omegas = np.sqrt(ks ** 2 + .0002 * ks ** 4)
        self.setData(self.X, np.dot(self.Ys.T, np.cos(self._phase * omegas)))

    def keyPressEvent(self, event):
        text = event.text()
        if text == ' ':
            if self.timer.isActive():
                self.timer.stop()
            else:
                self.timer.start(self.interval)
        elif text == 'r':
            self.timer.stop()
            del self.timer
            # self.widget.clear()
            self.setup(self.wavetype)
        elif text == 'c':
            nextwavetype = self.wavetypes[(
                self.wavetypes.index(self.wavetype) + 1) % len(self.wavetypes)]
            self.timer.stop()
            del self.timer
            # self.widget.clear()
            self.setup(nextwavetype)
        elif text == '+':
            self.timer.stop()
            self.rate *= 1.2
            print(self.rate)
            self.timer.start()
        elif text == '-':
            self.timer.stop()
            self.rate /= 1.2
            print(self.rate)
            self.timer.start()
        elif text == 'S':
            self.setupDialog()
        else:
            event.ignore()

    def setupDialog(self):
        if not self.setupWidget:
            self.setupWidget = SetupWidget()
            self.setupWidget.rejected.connect(self.setupWidget.hide)
            self.setupWidget.accepted.connect(self._setup)
            self.setupWidget.oH.setChecked(self.onlyOddHarmonics)
            self.setupWidget.hH.setChecked(self.suppressHigherHarmonics)
        self.setupWidget.selType.setCurrentIndex(
            WavePlot.wavetypes.index(self.wavetype))
        self.setupWidget.show()
        self.setupWidget.raise_()
        self.setupWidget.activateWindow()

    def _setup(self, **kwargs):
        if not self.setupWidget.kInp.hasAcceptableInput():
            QMessageBox.warning(self.setupWidget, 'UWAGA!',
                                'błędna wartość liczby harmonik')
            return
        self.onlyOddHarmonics = self.setupWidget.oH.isChecked()
        self.suppressHigherHarmonics = self.setupWidget.hH.isChecked()
        kInpText = self.setupWidget.kInp.text()
        if kInpText:
            self.kmax = int(kInpText)
        self.wavetype = self.setupWidget.selType.currentText()
        self.setup(**kwargs)
        self.setupWidget.hide()
        # self.timer.start(self.interval)


class SetupWidget(QDialog):
    def __init__(self, **kwargs):
        super(SetupWidget, self).__init__(**kwargs)
        self.oH = QCheckBox('tylko harmoniki nieparzyste?')
        self.hH = QCheckBox('tłumić wyższe harmoniki?')
        self.kInp = QLineEdit()
        self.kInp.setMaxLength(3)
        self.kInp.setPlaceholderText(str(plot.kmax))
        self.kInp.setValidator(QIntValidator(1, 999))
        self.selType = QComboBox(self)
        self.selType.addItems(WavePlot.wavetypes)
        self.selType.setInsertPolicy(QComboBox.NoInsert)

        self.bBox = QDialogButtonBox(QDialogButtonBox.Ok
                                     | QDialogButtonBox.Cancel)
        layout = QGridLayout()
        self.setLayout(layout)
        layout.addWidget(self.oH, 0, 0)
        layout.addWidget(self.hH, 1, 0)
        layout.addWidget(QLabel('liczba harmonik: '), 2, 0)
        layout.addWidget(self.kInp, 2, 1)
        layout.addWidget(QLabel('wybierz typ fali:'), 3, 0)
        layout.addWidget(self.selType, 4, 0)
        layout.addWidget(self.bBox, 5, 0)
        self.rejected, self.accepted = self.bBox.rejected, self.bBox.accepted


if __name__ == '__main__':
    app = QApplication(sys.argv)
    w = QMainWindow()
    w.setGeometry(100, 100, 800, 600)
    w.setWindowTitle('Ruch falowy we wnęce z dypersją - animacja')
    w.keyPressEvent = (
        lambda ev: app.quit() if ev.text() == 'q' else ev.ignore())
    plot = WavePlot()
    plot.setup(kmax=16, suppressHigherHarmonics=True, interval=25)
    chartView = QChartView(plot)
    chartView.setRenderHint(QPainter.Antialiasing)
    chartView.keyPressEvent = plot.keyPressEvent
    w.setCentralWidget(chartView)
    w.setWindowTitle('PyQtChart')
    w.keyPressEvent = (
        lambda ev: app.quit() if ev.text() == 'q' else ev.ignore())
    w.statusBar().showMessage(
        "PyQtChart w działaniu... naciśnij 'q' aby zakończyć; "
        "Spację aby zatrzymać / wznowić; "
        "'r' aby zacząć od początku; "
        "'c' aby zmienić typ fali; "
        "'+' przyspiesza, '-' spowalnia; "
        "'S' dla zmiany ustawień")
    w.show()
    # plot.timer.start(plot.interval)
    sys.exit(app.exec())
