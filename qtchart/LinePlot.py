#! /usr/bin/python3

from PyQt5.QtChart import QChart, QChartView, QLineSeries
from PyQt5.Qt import QPointF, QPainter, QPen, QColor
from PyQt5.QtWidgets import QApplication, QMainWindow
from numpy import array, min, max, linspace, sin, pi
from sys import argv, exit


class LinePlot(QChart):

    padding = .05

    def applyDefaultStyle(self):
        self.setTheme(QChart.ChartThemeDark)
        self.legend().hide()
        self.setDropShadowEnabled(True)
        # pen = QPen(QColor('yellow'))
        # self.series()[0].setPen(pen)

    def setPlotData(self, X, Y):
        self.series()[0].replace(
            [QPointF(x, y) for (x, y) in array([X, Y]).T])

    def setXrange(self, xmin, xmax):
        self.axisX().setRange(xmin, xmax)

    def setYrange(self, ymin, ymax):
        self.axisY().setRange(ymin, ymax)

    def setXtitle(self, text):
        self.axisX().setTitleText(text)

    def setYtitle(self, text):
        self.axisY().setTitleText(text)

    def plot(self,
             X,
             Y,
             lineWidth=None,
             lineColor=None,
             xGrid=None,
             yGrid=None):
        self.addSeries(QLineSeries())
        self.setPlotData(X, Y)
        self.createDefaultAxes()
        minX, maxX, minY, maxY = min(X), max(X), min(Y), max(Y)
        dX, dY = (1 + self.padding) * (maxX - minX), (1 + self.padding) * (maxY - minY)
        X0, Y0 = (maxX + minX) / 2, (maxY + minY) / 2
        self.setXrange(X0 - dX / 2, X0 + dX / 2)
        self.setYrange(Y0 - dY / 2, Y0 + dY / 2)
        self.applyDefaultStyle()
        if lineWidth:  # yes, it has to be this cumbersome
            series = self.series()[0]
            pen = series.pen()
            pen.setWidth(lineWidth)
            series.setPen(pen)
        if lineColor:
            self.series()[0].setColor(QColor(lineColor))
        if xGrid is not None:
            self.axisX().setGridLineVisible(bool(xGrid))
        if yGrid is not None:
            self.axisY().setGridLineVisible(bool(yGrid))
        self.chartView = QChartView(self)
        self.chartView.setRenderHint(QPainter.Antialiasing)


if __name__ == '__main__':

    X = linspace(0., 4., 200)
    Y = sin(pi * X)

    app = QApplication(argv)
    w = QMainWindow()
    lp = LinePlot()
    lp.plot(X, Y, lineWidth=3, lineColor='#ff6', yGrid=False)
    lp.setXtitle('<b><i>X</i></b>')
    lp.setYtitle('<b><i>Y</i></b>')
    lp.setTitle('<b style="font-size:16pt;">A Trigonometric Function</b>')
    # w = lp.chartView
    w.setWindowTitle('a LinePlot')
    w.setCentralWidget(lp.chartView)
    w.keyPressEvent = (
        lambda ev: app.quit() if ev.text() == 'q' else ev.ignore())
    w.statusBar().showMessage('© 2018 Robert J Budzyński')
    w.resize(800, 600)
    w.show()
    exit(app.exec())
