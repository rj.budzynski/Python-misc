#! /usr/bin/python3
#

from PyQt5.QtWidgets import QApplication, QMainWindow
from PyQt5.QtChart import QChart, QChartView, QLineSeries
from PyQt5.Qt import QPainter, QColor, QPen, QPointF
import sys
import numpy as np


app = QApplication(sys.argv)

# create the data

XY = np.zeros((100, 2), dtype=np.float)
XY[:, 0] = np.linspace(0., 2., 100)
XY[:, 1] = np.sin(np.pi * XY[:, 0])

XZ = np.zeros((100, 2), dtype=np.float)
XZ[:, 0] = np.linspace(0., 2., 100)
XZ[:, 1] = np.cos(np.pi * XZ[:, 0])

data = [XY, XZ]

# package the data

series = QLineSeries()
for x, y in data[0]:
    series.append(x, y)

# create the chart and widgets, set options

chart = QChart()
chart.addSeries(series)  # set the data
chart.createDefaultAxes()
chart.setTitle('<b style="font-size:16pt;">PyQtChart in action</b>')
chart.legend().hide()
chart.setTheme(QChart.ChartThemeDark)
pens = [QPen(QColor('red')), QPen(QColor('yellow'))]
pens[0].setStyle(3)  # cf. Qt.PenStyle
pens[1].setStyle(1)
pens[0].setWidth(9)
pens[1].setWidth(3)
series.setPen(pens[0])
# series.setColor(QColor('yellow'))  # can override pen color but no other properties
chart.setDropShadowEnabled(True)
chart.axisX().setTitleText('X')
chart.axisY().setTitleText('wychylenie')
chartView = QChartView(chart)
chartView.setRenderHint(QPainter.Antialiasing)


def togglePlots(ev, which=[0]):
    if ev.text() == ' ':
        which[0] = 0 if which[0] else 1
        series.setPen(pens[which[0]])
        series.replace([QPointF(x, y) for (x, y) in data[which[0]]])
        chartView.repaint()
    else:
        ev.ignore()


chartView.keyPressEvent = togglePlots

mainWindow = QMainWindow()
mainWindow.setCentralWidget(chartView)
mainWindow.setWindowTitle('PyQtChart')
mainWindow.keyPressEvent = (
    lambda ev: app.quit() if ev.text() == 'q' else ev.ignore())
mainWindow.statusBar().showMessage('© 2018 Robert J Budzyński')
mainWindow.resize(800, 600)
mainWindow.show()

app.exec()
