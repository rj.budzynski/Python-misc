#! /usr/bin/python3

from PyQt5.QtChart import QChart, QChartView, QScatterSeries
from PyQt5.Qt import QPointF, QPainter, QColor
from PyQt5.QtWidgets import QApplication, QMainWindow
from numpy import array, min, max, linspace, sin, pi, random
from sys import argv, exit


class ScatterPlot(QChart):

    padding = .05

    def applyDefaultStyle(self):
        self.setTheme(QChart.ChartThemeDark)
        self.legend().hide()
        self.setDropShadowEnabled(True)
        # pen = QPen(QColor('yellow'))
        # self.series()[0].setPen(pen)

    def setPlotData(self, X, Y, index=0):
        self.series()[index].replace(
            [QPointF(x, y) for (x, y) in array([X, Y]).T])

    def setXrange(self, xmin, xmax):
        self.axisX().setRange(xmin, xmax)

    def setYrange(self, ymin, ymax):
        self.axisY().setRange(ymin, ymax)

    def setXtitle(self, text):
        self.axisX().setTitleText(text)

    def setYtitle(self, text):
        self.axisY().setTitleText(text)

    def plot(self,
             X,
             Y,
             markerSize=5.,
             markerShape=None,
             markerBorderColor=None,
             xGrid=None,
             yGrid=None):
        self.addSeries(QScatterSeries())
        self.setPlotData(X, Y)
        self.createDefaultAxes()
        minX, maxX, minY, maxY = min(X), max(X), min(Y), max(Y)
        dX, dY = (1 + self.padding) * (maxX - minX), (1 + self.padding) * (
            maxY - minY)
        X0, Y0 = (maxX + minX) / 2, (maxY + minY) / 2
        self.setXrange(X0 - dX / 2, X0 + dX / 2)
        self.setYrange(Y0 - dY / 2, Y0 + dY / 2)
        self.applyDefaultStyle()
        self.series()[0].setMarkerSize(markerSize)
        if markerShape is not None:  # should be 0 or 1
            self.series()[0].setMarkerShape(markerShape)
        if markerBorderColor is not None:
            self.series()[0].setBorderColor(QColor(markerBorderColor))
        if xGrid is not None:
            self.axisX().setGridLineVisible(bool(xGrid))
        if yGrid is not None:
            self.axisY().setGridLineVisible(bool(yGrid))
        self.chartView = QChartView(self)
        self.chartView.setRenderHint(QPainter.Antialiasing)

    def addPlot(self,
                X,
                Y,
                markerSize=5.,
                markerShape=None,
                markerBorderColor=None):
        index = len(self.series())
        self.addSeries(QScatterSeries())
        self.setPlotData(X, Y, index)
        minX, maxX = self.axisX().min(), self.axisX().max()
        minY, maxY = self.axisY().min(), self.axisY().max()
        minX, maxX = min((minX, min(X))), max((maxX, max(X)))
        minY, maxY = min((minY, min(Y))), max((maxY, max(Y)))
        self.setXrange(minX, maxX)
        self.setYrange(minY, maxY)
        self.series()[index].attachAxis(self.axisX())
        self.series()[index].attachAxis(self.axisY())
        self.series()[index].setMarkerSize(markerSize)
        if markerShape is not None:  # should be 0 or 1
            self.series()[index].setMarkerShape(markerShape)
        if markerBorderColor is not None:
            self.series()[index].setBorderColor(QColor(markerBorderColor))


if __name__ == '__main__':

    X = linspace(0., 4., 50)
    Y = sin(pi * X)
    X = random.normal(size=99)
    Y = random.normal(size=99)
    X2 = 2 + random.normal(size=99)
    Y2 = X2 + .3 * random.normal(size=99)

    app = QApplication(argv)
    w = QMainWindow()
    sp = ScatterPlot()
    sp.plot(X, Y, markerBorderColor='orange', markerShape=1, markerSize=3.)
    sp.addPlot(X2, Y2, markerBorderColor='magenta', markerSize=6.)
    sp.setXtitle('<b><i>X</i></b>')
    sp.setYtitle('<b><i>Y</i></b>')
    sp.setTitle('<b style="font-size:16pt;">A ScatterPlot</b>')
    # w = lp.chartView
    w.setWindowTitle('PyQt5.QtChart')
    w.setCentralWidget(sp.chartView)
    w.keyPressEvent = (
        lambda ev: app.quit() if ev.text() == 'q' else ev.ignore())
    w.statusBar().showMessage('© 2018 Robert J Budzyński')
    w.resize(800, 600)
    w.show()
    exit(app.exec())
