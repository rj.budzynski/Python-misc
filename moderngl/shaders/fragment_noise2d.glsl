#version 330

uniform float seed;
uniform vec2 pxl_size;

out vec4 fragColor;

${randomDefs}

void main() {
    vec2 uv = floor(gl_FragCoord.xy / pxl_size);
    float r = random(vec2(seed) + uv);
    fragColor = vec4(
        0., 0., 0., r
    );
}