vec3 hsv_to_rgb(vec3 hsv) {
    float v = hsv.z;
    if (hsv.y == 0.) return vec3(v, v, v);
    float f = fract(6. * hsv.x);
    float p = v * (1. - hsv.y);
    float q = v * (1. - f * hsv.y);
    float t = v * (1. - hsv.y * (1. - f));
    int i = int(floor(6. * hsv.x)) % 6; 
    switch (i) {
        case 0: return vec3(v, t, p);
        case 1: return vec3(q, v, p);
        case 2: return vec3(p, v, t);
        case 3: return vec3(p, q, v);
        case 4: return vec3(t, p, v);
        case 5: return vec3(v, p, q);
    }
}