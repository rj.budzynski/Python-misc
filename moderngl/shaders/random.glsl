/* see: http://amindforeverprogramming.blogspot.com/2013/07/random-floats-in-glsl-330.html */

#define MANTISSA_MASK 0x007FFFFFu
#define FLOAT_ONE     0x3F800000u


uint hash( uint x ) {
    x += ( x >> 10u );
    x ^= ( x <<  6u );
    x += ( x >>  3u );
    x ^= ( x << 11u );
    x += ( x >> 15u );
    return x;
}

uint hash(uvec2 xy) {
    uint x = xy.x;
    uint y = xy.y;
    x += x >> 11;
    x ^= x << 7;
    x += y;
    x ^= x << 6;
    x += x >> 15;
    x ^= x << 5;
    x += x >> 12;
    x ^= x << 9;
    return x;
}

float random( float f ) {
    uint h = hash(floatBitsToUint(f));
    h &= MANTISSA_MASK;
    h |= FLOAT_ONE;

    float  r2 = uintBitsToFloat( h );
    return r2 - 1.;
}
 
float random(vec2 uv) {
    uvec2 bits = floatBitsToUint(uv);
    uint h = hash(bits);
    h &= MANTISSA_MASK;
    h |= FLOAT_ONE;

    float r2 = uintBitsToFloat(h);
    return r2 - 1.;   
}