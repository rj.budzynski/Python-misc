#! /usr/bin/python3

from enum import Enum
import numpy as np
from math import log
import moderngl as gl
from PyQt5.QtWidgets import (QApplication, QOpenGLWidget, QLabel, QVBoxLayout,
                             QDialog, QDialogButtonBox, QGridLayout, QComboBox,
                             QDoubleSpinBox)
from PyQt5.QtGui import QSurfaceFormat
from PyQt5.QtCore import Qt, QTimer, QElapsedTimer

# size of the lattice
COLS, ROWS = 700, 700

# physical parameters
DEFAULT_TEMPERATURE = 1.01  # in units of the critical Onsager temperature
DEFAULT_FIELD = 0.0001  # including Boltzmann factor

# algorithms


class Algorithm(Enum):
    METROPOLIS, HEATBATH, GLAUBER = 1, 2, 3


# DEFAULT_ALGORITHM = Algorithm.GLAUBER


class GLIsingWindow(QOpenGLWidget):

    def __init__(self):

        super().__init__()
        self.label1 = QLabel()
        self.layout = QVBoxLayout(self)
        self.layout.addWidget(self.label1, 0, Qt.AlignLeft | Qt.AlignTop)
        self.label2 = QLabel()
        self.layout.addWidget(self.label2, 0, Qt.AlignLeft | Qt.AlignTop)
        self.layout.addStretch(12)
        self.setToolTip('click to pause / restart')
        fmt = QSurfaceFormat()
        fmt.setVersion(3, 3)
        fmt.setProfile(QSurfaceFormat.CoreProfile)
        fmt.setSwapInterval(1)
        self.setFormat(fmt)
        self.setupWidget = None
        self.transform_prog = None

    def getTemperature(self):

        if self.transform_prog:
            return log(1 + 2 ** 0.5) / 2 / self.transform_prog['J'].value
        return self._temperature

    def setTemperature(self, temp):

        self._temperature = temp
        if self.transform_prog:
            self.transform_prog['J'] = log(1 + 2 ** 0.5) / 2 / temp
            self.label1.setText(
                '<p style="color: gold;font-size: x-large;font-family:Courier New,fixed;font-weight:900;">'
                f'{COLS * ROWS} spins; Temp.: {self.temperature:.3f};'
                f' ext. field: {self.externalField:.3g}')

    temperature = property(getTemperature, setTemperature)

    def getExternalField(self):

        if self.transform_prog:
            return self.transform_prog['h'].value
        return self._h

    def setExternalField(self, h):

        self._h = h
        if self.transform_prog:
            self.transform_prog['h'] = h
            self.label1.setText(
                '<p style="color: gold;font-size: x-large;font-family:Courier New,fixed;font-weight:900;">'
                f'{COLS * ROWS} spins; Temp.: {self.temperature:.3f};'
                f' ext. field: {self.externalField:.3g}')

    externalField = property(getExternalField, setExternalField)

    def setupDialog(self):

        if not self.setupWidget:
            self.setupWidget = SetupWidget()
            self.setupWidget.rejected.connect(self._cancel)
            self.setupWidget.accepted.connect(self._setup)
        self.setupWidget.show()
        # self.setupWidget.raise_()
        self.setupWidget.activateWindow()

    def _cancel(self):

        self.app.quit()

    def _setup(self):

        self.algorithm = Algorithm[self.setupWidget.selectAlgorithm.currentText(
        )]
        self.temperature = self.setupWidget.inputTemp.value()
        self.externalField = self.setupWidget.inputField.value()
        self.setWindowTitle(f'2D Ising model: {self.algorithm.name} algorithm')
        self.setupWidget.hide()
        self.show()

    def paintGL(self):

        self.ctx = gl.create_context()
        self.screen = self.ctx.detect_framebuffer()
        self.init()
        self.render()
        self.paintGL = self.render

    def init(self):

        self.display_prog = self.ctx.program(
            vertex_shader='''#version 330

            in vec2 in_vert;
            out vec2 v_text;

            void main() {
                v_text = (in_vert + 1.) / 2.;
                gl_Position = vec4(in_vert, 0., 1.);
            }
            ''',
            fragment_shader='''#version 330

            uniform sampler2D Texture;

            in vec2 v_text;
            out vec4 f_color;

            void main() {
                f_color = texture(Texture, v_text);
            }
            ''')

        self.transform_prog = self.ctx.program(
            vertex_shader='''#version 330
            #define METROPOLIS 1
            #define HEATBATH 2
            #define GLAUBER 3

            uniform int Algorithm;
            uniform sampler2D Texture;
            uniform float J, h;
            uniform int step;
            uniform float seed;
            out float out_bit;

            /* see: http://amindforeverprogramming.blogspot.com/2013/07/random-floats-in-glsl-330.html */

            uint hash( uint x ) {
                x += ( x >> 11u );
                x ^= ( x <<  7u );
                x += ( x >>  15u );
                x ^= ( x << 5u );
                x += ( x >> 12u );
                x ^= ( x << 9u );
                return x;
            }

            float random( float f ) {
                const uint mantissaMask = 0x007FFFFFu;
                const uint one          = 0x3F800000u;

                uint h = hash( floatBitsToUint( f ) );
                h &= mantissaMask;
                h |= one;

                float  r2 = uintBitsToFloat( h );
                return r2 - 1.0;
            }

            float sum_of_neighbors(int x, int y) {
                ivec2 tSize = textureSize(Texture, 0);
                return texelFetch(Texture, ivec2((x + 1) % tSize.x, y), 0).a
                    + texelFetch(Texture, ivec2(x, (y + 1) % tSize.y), 0).a
                    + texelFetch(Texture, ivec2((x -1 + tSize.x) % tSize.x, y), 0).a
                    + texelFetch(Texture, ivec2(x, (y - 1 + tSize.y) % tSize.y), 0).a;
            }

            void main() {
                int cols = textureSize(Texture, 0).x;
                int x = gl_VertexID % cols;
                int y = gl_VertexID / cols;
                float bit = texelFetch(Texture, ivec2(x, y), 0).a;
                if (((x % 2) ^ (y % 2)) == step) {
                    float spin = 2. * bit - 1.;
                    float dE = 2. * sum_of_neighbors(x, y) - 4.;
                    dE *= 2. * J;
                    dE += 2. * h;
                    float r = 1. - random(seed + random(seed * float(gl_VertexID)));
                    switch (Algorithm) {
                        case METROPOLIS:
                            dE *= spin;
                            if (dE < 0. || dE < -log(r)) {
                                bit = (bit == 0. ? 1. : 0.);
                            }
                            break;
                        case HEATBATH:
                            r = 1. / (1. - r) - 1.;
                            bit = dE > r - 1. || dE > log(r) ? 1. : 0.;
                            break;
                        case GLAUBER:
                            dE *= spin;
                            r = 1. / (1. -r) - 1.;
                            if (dE < log(r)) bit = (bit == 0. ? 1. : 0.);
                            break;
                    }                
                }
                out_bit = bit;
            }
            ''',
            varyings=['out_bit'])

        # set constant uniforms if any

        self.temperature = self._temperature
        self.externalField = self._h

        # one of METROPOLIS, HEATBATH or GLAUBER
        # self.algorithm = DEFAULT_ALGORITHM
        self.transform_prog['Algorithm'] = self.algorithm.value

        # make some random pixels

        pixels = np.random.randint(0, 2, size=(
            self.width(), self.height())).astype('float32')

        self.npixels = pixels.size
        self.averageMagnetization = 2. * \
            pixels.mean() - 1.  # this will be average over time

        # setup texture

        self.texture = self.ctx.texture(
            pixels.shape, 1, pixels.tobytes(), dtype='f4')
        self.texture.filter = gl.NEAREST, gl.NEAREST
        self.texture.swizzle = '000R'

        # set vbo & vao

        self.vbo = self.ctx.buffer(np.array([
            -1.0, -1.0,  # lower left
            -1.0, 1.0,  # upper left
            1.0, -1.0,  # lower right
            1.0, 1.0    # upper right
        ], dtype='float32'))
        self.vao = self.ctx.vertex_array(
            self.display_prog, self.vbo, 'in_vert')

        # setup for the transform step

        self.tao = self.ctx.vertex_array(self.transform_prog, [])
        self.pbo = self.ctx.buffer(reserve=pixels.nbytes)

        # setup timers if any

        self.timer = QTimer()
        self.timer.timeout.connect(self.update)
        self.infoTimer = QElapsedTimer()
        self.frameTimer = QElapsedTimer()
        self.runTime = 0.
        self.transform_prog['step'] = 0
        self.transform_prog['seed'] = 0
        self.paused = True
        self.updateCount = 0
        self.totalUpdateCount = 0

        # GUI stuff
        self.label1.setText(
            '<p style="color: gold;font-size: x-large;font-family:Courier New,fixed;font-weight:800;">'
            f'{self.npixels} spins; Temp.: {self.temperature:.3f};'
            f' ext. field: {self.externalField:.3g}')

    def render(self):

        self.ctx.clear(1.0, 0.1, 1.0, 1.0)
        self.texture.use(location=0)
        self.ctx.enable(gl.BLEND)

        # render from the texture
        self.vao.render(gl.TRIANGLE_STRIP)  # or the mode of your choice

        # info extraction and presentation

        if self.infoTimer.hasExpired(1000):
            pixels = np.frombuffer(self.texture.read(), dtype='float32')
            magnetization = 2. * pixels.mean() - 1.
            self.averageMagnetization = (
                self.averageMagnetization * (self.totalUpdateCount or 1)
                + magnetization * self.updateCount)
            self.totalUpdateCount += self.updateCount
            self.averageMagnetization /= (self.totalUpdateCount or 1)
            if self.infoTimer.isValid():
                self.runTime += self.infoTimer.elapsed()
            self.label2.setText(
                '<p style="color: gold;font-size: x-large;font-family:Courier New,fixed;font-weight:800;">'
                f'M: {magnetization * 100.:6.2f}%; &lt;M&gt;: {self.averageMagnetization * 100.:6.2f}%'
                '; updates/s: '
                f'{self.updateCount and round(1e3*self.updateCount / self.infoTimer.restart())}'
                f'; updates: {self.totalUpdateCount}')
            # f'; runtime: {self.runTime * 1e-3:.0f} secs</p>')
            self.updateCount = 0

        if self.paused:
            return

        # do the transform

        while self.frameTimer.elapsed() < 30:
            self.transform_prog['seed'] = np.random.random()

            self.tao.transform(self.pbo, vertices=self.npixels)
            self.texture.write(self.pbo)
            self.transform_prog['step'].value ^= 1
            self.tao.transform(self.pbo, vertices=self.npixels)
            self.texture.write(self.pbo)
            self.transform_prog['step'].value ^= 1
            self.updateCount += 1

        self.frameTimer.restart()

    def mouseReleaseEvent(self, event):

        if self.paused:
            self.timer.start(0)
            self.infoTimer.start()
            self.frameTimer.start()
        else:
            self.timer.stop()
            if self.infoTimer.isValid():
                self.runTime += self.infoTimer.elapsed()
            self.infoTimer.invalidate()
            self.frameTimer.invalidate()
            self.setupDialog()
        self.paused = not self.paused


class SetupWidget(QDialog):

    def __init__(self):

        super().__init__()

        self.setModal(True)
        self.label1 = QLabel()
        self.label1.setText('setup parameters')
        self.label1.setAlignment(Qt.AlignCenter)
        self.label2 = QLabel()
        self.label2.setText('(click on canvas to start/pause/resume)')
        self.label2.setAlignment(Qt.AlignCenter)
        self.label3 = QLabel()
        self.label3.setText('select an update algorithm:')
        self.label4 = QLabel()
        self.label4.setText('temperature:')
        self.label4.setToolTip('in units of the critical temperature')
        self.label4.setAlignment(Qt.AlignRight)
        self.label5 = QLabel()
        self.label5.setText('external field:')
        self.label5.setToolTip('includes Boltzmann factor')
        self.label5.setAlignment(Qt.AlignRight)
        self.inputTemp = QDoubleSpinBox()
        self.inputTemp.setDecimals(4)
        self.inputTemp.setSingleStep(0.001)
        self.inputTemp.setValue(1.)
        self.inputField = QDoubleSpinBox()
        self.inputField.setDecimals(4)
        self.inputField.setRange(-1., 1.)
        self.inputField.setSingleStep(0.001)
        self.inputField.setValue(0.001)
        self.selectAlgorithm = QComboBox()
        self.selectAlgorithm.addItems([alg.name for alg in Algorithm])
        self.bBox = QDialogButtonBox(QDialogButtonBox.Ok
                                     | QDialogButtonBox.Cancel)
        self.rejected, self.accepted = self.bBox.rejected, self.bBox.accepted
        layout = QGridLayout()
        self.setLayout(layout)
        layout.addWidget(self.label1, 0, 0, 1, 2, Qt.AlignCenter)
        layout.addWidget(self.label2, 1, 0, 1, 2, Qt.AlignCenter)
        layout.addWidget(self.label3, 2, 0)
        layout.addWidget(self.selectAlgorithm, 2, 1)
        layout.addWidget(self.label4, 3, 0)
        layout.addWidget(self.inputTemp, 3, 1)
        layout.addWidget(self.label5, 4, 0)
        layout.addWidget(self.inputField, 4, 1)
        layout.addWidget(self.bBox, 5, 1, Qt.AlignCenter)


if __name__ == '__main__':

    from sys import argv, exit

    app = QApplication(argv)
    mw = GLIsingWindow()
    mw.resize(COLS, ROWS)
    # make the window nonresizable
    mw.setMinimumSize(mw.size())
    mw.setMaximumSize(mw.size())
    mw.setWindowTitle('2D Ising Model')
    mw.setupDialog()
    mw.app = app
    exit(app.exec_())
