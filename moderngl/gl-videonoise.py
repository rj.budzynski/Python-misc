#! /usr/bin/python3
"""
A toy for testing pseudorandom number generation within a shader.
"""

import string
import numpy as np
import moderngl as gl
from PyQt5.QtWidgets import QApplication, QOpenGLWidget
from PyQt5.QtGui import QSurfaceFormat
from PyQt5.QtCore import QTimer


class MyWindow(QOpenGLWidget):

    VERTEX_SHADER = 'shaders/vertex2d.glsl'
    FRAGMENT_SHADER = 'shaders/fragment_noise2d.glsl'
    INCLUDE_SHADER = 'shaders/random.glsl'

    paused = True
    # delay in milliseconds between frames
    # 0.: run at max. synced framerate
    DELAY = 0
    # size of a pixel block (W, H)
    # determines the picture's "granularity"
    PXL_SIZE = 1, 1

    def __init__(self):

        super().__init__()
        fmt = QSurfaceFormat()
        fmt.setVersion(3, 3)
        fmt.setProfile(QSurfaceFormat.CoreProfile)
        fmt.setSwapInterval(1)
        self.setFormat(fmt)
        self.timer = QTimer()
        self.timer.timeout.connect(self.update)
        self.mouseReleaseEvent = self.togglePaused
        self.setToolTip('mouse click pauses/restarts')

    def paintGL(self):

        self.init()
        self.paintGL = self.render

    def init(self):

        self.ctx = gl.create_context()
        self.prog = self.ctx.program(
            vertex_shader=open(self.VERTEX_SHADER).read(),
            fragment_shader=string.Template(
                open(self.FRAGMENT_SHADER).read()
            ).substitute(randomDefs=open(self.INCLUDE_SHADER).read()),
        )

        # set uniforms if any
        self.prog['pxl_size'] = self.PXL_SIZE
        # self.prog['cols'] = self.width() // self.PXL_SIZE[0]

        # set vbo & vao

        vertices = np.array(
            [-1, -1, -1, 1, 1, -1, 1, 1],  # two triangles forming a quad
            dtype='float32',
        )

        self.vbo = self.ctx.buffer(vertices.tobytes())
        self.vao = self.ctx.vertex_array(self.prog, self.vbo, 'in_vert')

        # The viewport is somehow set automagically
        # self.ctx.viewport = 0, 0, self.width(), self.height()

    def render(self):

        self.prog['seed'] = np.random.random()
        self.ctx.clear(1.0, 1.0, 1.0)
        self.ctx.enable(gl.BLEND)
        self.vao.render(gl.TRIANGLE_STRIP)  # or the mode of your choice

    def togglePaused(self, event):

        if self.paused:
            self.timer.start(self.DELAY)
        else:
            self.timer.stop()
        self.paused = not self.paused


if __name__ == '__main__':

    from sys import argv, exit

    app = QApplication(argv)
    mw = MyWindow()
    mw.resize(800, 600)
    mw.setWindowTitle('GL random noise')
    mw.show()
    exit(app.exec_())
