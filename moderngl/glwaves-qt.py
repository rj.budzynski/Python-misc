#! /usr/bin/python3

import numpy as np
import moderngl as gl
from qtmoderngl import QModernGLWidget
from PyQt5.QtWidgets import QApplication, QMainWindow, QLabel, QHBoxLayout
from PyQt5.QtCore import QTimer, QElapsedTimer, Qt
from colorsys import hsv_to_rgb
from string import Template


class MyWidget(QModernGLWidget):

    def __init__(self, NWAVES=None):

        self.NWAVES = NWAVES or 5
        super().__init__()
        self.scene = None
        self.label = QLabel()
        self.layout = QHBoxLayout(self)
        self.layout.addWidget(self.label, 0, Qt.AlignLeft | Qt.AlignTop)

    def init(self):

        NWAVES = self.NWAVES
        self.prog = self.ctx.program(
            vertex_shader='''#version 330

            in vec2 in_vert;

            void main() {
                gl_Position = vec4(in_vert, 0., 1.);
            }
            ''',
            fragment_shader=Template('''#version 330

            #define PI radians(180.)
            #define NWAVES ${NWAVES}

            uniform float time;
            uniform vec2 vtrigs[NWAVES];
            out vec4 fragColor;

            void main() {
                float phase = -time;
                float amplitude = 0.;
                float A = PI / float(NWAVES);
                vec2 uv = gl_FragCoord.xy;

                for (int i = 0; i < NWAVES; ++i) {
                    amplitude += sin(dot(vtrigs[i], uv) / 5. + phase);
                }
                amplitude += float(NWAVES);
                amplitude = mod(floor(amplitude), 2.) == 0. ?
                                fract(amplitude) :
                                fract(1. - amplitude);
                fragColor = vec4(0., 0., 0., 1.) * amplitude;
            }
            ''').substitute(NWAVES=NWAVES)
        )

        vtrigs = []
        for k in range(NWAVES):
            angle = k*np.pi/NWAVES
            vtrigs.append((np.cos(angle), np.sin(angle)))

        self.prog['vtrigs'].value = vtrigs

        vertices = np.array([  # two triangles forming a quad
            -1, -1, -1, 1,
            1, -1, 1, 1
        ], dtype='float32')

        self.vbo = self.ctx.buffer(vertices.tobytes())
        self.vao = self.ctx.vertex_array(self.prog, self.vbo, 'in_vert')

        self.time = self.prog['time']

        self.timer = QTimer()
        self.timer.timeout.connect(self.update)
        self.elapsedTimer = QElapsedTimer()
        self.elapsedTimer.start()
        self.fpsTimer = QElapsedTimer()
        self.timer.start(0)
        self.frames = 0
        self.fpsTimer.start()

    def render(self):

        if self.fpsTimer.hasExpired(3000):
            self.label.setText(
                '<b style="color:gold;font-size:x-large;">'
                f'FPS: {1e3 * self.frames / self.fpsTimer.restart():.1f}</b>')
            self.frames = 0
        else:
            self.frames += 1
        runtime = self.elapsedTimer.elapsed() * 1e-3
        self.time.value = runtime
        self.ctx.clear(*hsv_to_rgb((np.sin(runtime * 0.002) + 1) / 2, .75, 1.))
        self.ctx.enable(gl.BLEND)
        self.vao.render(gl.TRIANGLE_STRIP)


class MyWindow(QMainWindow):

    def __init__(self):

        super().__init__()
        self.setGeometry(100, 50, 600, 600)
        self.setWindowTitle('A Basic ModernGL + Qt5 Example')

    def keyPressEvent(self, event):

        key = event.key()
        if key == Qt.Key_F11:
            if self.isFullScreen():
                self.showNormal()
            else:
                self.showFullScreen()
        elif key == Qt.Key_Escape:
            if self.isFullScreen():
                self.showNormal()
            else:
                event.ignore()
        else:
            event.ignore()


if __name__ == '__main__':

    from sys import argv, exit
    NWAVES = None
    for a in argv[1:]:
        if a.isdigit():
            NWAVES = int(a)
            break
    app = QApplication(argv)
    mw = MyWindow()
    widget = MyWidget(NWAVES)
    mw.setCentralWidget(widget)
    mw.statusBar().showMessage('F11 toggles fullscreen mode')
    mw.show()
    exit(app.exec_())
