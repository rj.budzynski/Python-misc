from PyQt5 import QtGui, QtWidgets, QtCore

import moderngl

# pylint: disable=E0202


class QModernGLWidget(QtWidgets.QOpenGLWidget):

    def __init__(self):

        super(QModernGLWidget, self).__init__()
        fmt = QtGui.QSurfaceFormat()
        fmt.setVersion(3, 3)
        fmt.setProfile(QtGui.QSurfaceFormat.CoreProfile)
        fmt.setSwapInterval(1)
        self.setFormat(fmt)

    def initializeGL(self):

        ...

    def paintGL(self):

        self.ctx = moderngl.create_context()
        self.screen = self.ctx.detect_framebuffer()
        self.init()
        self.render()
        self.paintGL = self.render

    def init(self):

        'Implement in subclass'

    def render(self):

        'Implement in subclass'


