#! /usr/bin/python3

import numpy as np
import moderngl as gl
from PyQt5.QtWidgets import QApplication, QLabel, QOpenGLWidget, QVBoxLayout
from PyQt5.QtGui import QSurfaceFormat
from PyQt5.QtCore import QElapsedTimer, QTimer, Qt
from string import Template

TWOPI = np.pi * 2.


class config:
    """
    All configurable parameters are here.
    """

    # set the dimensions in natural units, ie. interaction radius = 1
    W: int = 32
    H: int = 32
    dimensions = np.array((W, H))
    # number of particles
    N = 3999
    # velocity in terms of displacement per update step
    V: float = .1
    # how strongly the direction of motion jitters
    noise: float = .3
    # size of a particle in pixels
    point_size: float = 5.
    # ms between frames, 0: fps at vsync rate if possible
    frame_delay: int = 0
    # set to False to switch off interactions
    interacting: bool = True
    # scale the dimensions to pixels
    scale = 22


vshader_source = """#version 330

uniform vec2 dimensions;
uniform sampler2D in_texture;
out float angle;

void main() {
    vec3 in_vert = texelFetch(in_texture, ivec2(gl_VertexID, 0), 0).xyz;
    angle = in_vert.z;
    vec2 pos = 2. * in_vert.xy / dimensions - 1.;
    gl_Position = vec4(pos.xy, 0., 1.);
}
"""

fshader_source = Template("""#version 330
#define TWOPI radians(360.)
out vec4 fragColor;
in float angle;

${colorDefs}

void main() {
    float r = 2. * distance(gl_PointCoord, vec2(.5, .5));
    float alpha = smoothstep(-1., -.85, -r);
    float brightness = .2 + .8 * smoothstep(-1., -.2, -r);
    fragColor = vec4(hsv_to_rgb(vec3(angle / TWOPI, 1., brightness)), alpha);
}
""").substitute(colorDefs=open('shaders/colorDefs.glsl').read())

shift_shader_source = """#version 330

uniform vec2 dimensions;
uniform float shift;
uniform sampler2D in_texture;
out vec3 out_vert;

void main() {
    vec3 in_vert = texelFetch(in_texture, ivec2(gl_VertexID, 0), 0).xyz;
    float angle = in_vert.z;
    vec2 vert = in_vert.xy + shift * vec2(cos(angle), sin(angle));
    vert = fract((vert + dimensions) / dimensions) * dimensions;
    out_vert = vec3(vert, angle);
}
"""

align_shader_source = Template("""#version 330
#define TWOPI radians(360.)

uniform bool interacting;
uniform float noise;
uniform float seed;
uniform sampler2D in_texture;
out vec3 out_vert;

${randomDefs}

void main() {
    vec3 in_vert = texelFetch(in_texture, ivec2(gl_VertexID, 0), 0).xyz;
    float angle = in_vert.z;
    vec2 vert = in_vert.xy;
    if (interacting) {
        int nearby = 0;
        float mean_c = 0., mean_s = 0.;
        for (int i = 0; i < textureSize(in_texture, 0).x; ++i){
            vec3 tex = texelFetch(in_texture, ivec2(i, 0), 0).xyz;
            vec2 xy = tex.xy;
            vec2 dr = vert - xy;
            vec2 dr2 = dr*dr;
            if (dr2.x + dr2.y < 1.) {
                float next_angle = tex.z;
                mean_c = (mean_c * nearby + cos(next_angle)) / (nearby + 1);
                mean_s = (mean_s * nearby + sin(next_angle)) / (nearby + 1);
                ++nearby;
            }
        }
        angle = atan(mean_s, mean_c);
    }
    angle += (random(dot(vec2(seed, angle), vert)) - .5) * TWOPI * noise;
    angle = fract(angle / TWOPI) * TWOPI;
    out_vert = vec3(vert, angle);
}
""").substitute(randomDefs=open('shaders/random.glsl').read())


class GLVicsekWidget(QOpenGLWidget, config):

    def __init__(self):

        super().__init__()
        fmt = QSurfaceFormat()
        fmt.setVersion(3, 3)
        fmt.setProfile(QSurfaceFormat.CoreProfile)
        fmt.setSwapInterval(1)
        self.setFormat(fmt)
        self.resize(self.scale * self.W, self.scale * self.H)
        self.setMaximumSize(self.size())
        self.setMinimumSize(self.size())
        self.setWindowTitle('Vicsek Model in GL')
        self.clock = QElapsedTimer()
        self.timer = QTimer()
        self.paused = True
        self.label1 = QLabel()
        self.layout = QVBoxLayout(self)
        self.layout.addWidget(self.label1, 0, Qt.AlignLeft | Qt.AlignTop)
        self.label2 = QLabel()
        self.layout.addWidget(self.label2, 0, Qt.AlignLeft | Qt.AlignTop)
        self.layout.addStretch(12)
        self.setToolTip('click to pause / resume')

    def paintGL(self):

        self.setup()
        self.paintGL = self.render

    @classmethod
    def initial_conditions(cls, N, ball=True):

        angles = np.random.random(N).astype('float32') * TWOPI
        if ball:
            thetas = np.random.random(N).astype('float32') * TWOPI
            radii = .3 * cls.dimensions.min() * np.sqrt(
                np.random.random(N).astype('float32'))
            posx = np.cos(thetas) * radii + cls.dimensions[0] / 2
            posy = np.sin(thetas) * radii + cls.dimensions[1] / 2
        else:
            posx, posy = (np.random.random(
                (N, 2)) * cls.dimensions).astype('float32').T
        return np.dstack((posx, posy, angles)).reshape((N, 3))

    def setup(self):

        self.ctx = gl.create_context()
        self.ctx.enable(gl.BLEND)
        self.ctx.point_size = self.point_size

        # PROGRAMS
        self.render_prog = self.ctx.program(vertex_shader=vshader_source,
                                            fragment_shader=fshader_source)
        self.shift_prog = self.ctx.program(vertex_shader=shift_shader_source,
                                           varyings=['out_vert'])
        self.align_prog = self.ctx.program(vertex_shader=align_shader_source,
                                           varyings=['out_vert'])

        # CONSTANT UNIFORMS
        self.render_prog['dimensions'] = self.W, self.H
        self.shift_prog['dimensions'] = self.render_prog['dimensions'].value
        # TOGGLE THIS TO SWITCH INTERACTIONS ON/OFF
        self.align_prog['interacting'] = self.interacting

        self.align_prog['noise'] = self.noise
        self.shift_prog['shift'].value = self.V

        # INITIAL CONDITIONS
        vertices = self.initial_conditions(self.N, ball=False)

        # INPUT ATTRIBUTES / TEXTURE
        self.in_texture = self.ctx.texture((len(vertices), 1),
                                           3,
                                           vertices.tobytes(),
                                           dtype='f4')
        self.vao = self.ctx.vertex_array(self.render_prog, [])
        self.tao = self.ctx.vertex_array(self.shift_prog, [])
        self.tao2 = self.ctx.vertex_array(self.align_prog, [])
        self.out_texture = self.ctx.texture((len(vertices), 1), 3, dtype='f4')

        # OUTPUT BUFFER FOR TRANSFORM PROG
        self.out_buffer = self.ctx.buffer(reserve=vertices.nbytes)

        # SOME COSMETICS
        self.setWindowTitle(
            f'2D Vicsek model in GL: {len(vertices)} particles')
        self.label1.setText(
            '<p style="color: gold;font-size: x-large;font-family:Courier New,fixed;font-weight:600;">'
            f'order param.: {self.order_parameter:.3f}')

        self.clock.start()
        self.runtime = 0
        self.frames = 0
        self.timer.timeout.connect(self.update)
        self.timer.start(self.frame_delay)

    @property
    def order_parameter(self):
        """
        Length of the average step displacement vector in units of 
        the speed.
        """
        angles = np.frombuffer(self.in_texture.read(), dtype='float32')[2::3]
        return np.hypot(np.cos(angles).mean(), np.sin(angles).mean())

    @property
    def volume(self):

        return self.dimensions.prod()

    @property
    def particle_density(self):
        """
        Number of particles per interaction radius squared.
        """
        return self.in_texture.width / self.dimensions.prod()

    @property
    def speed(self):
        """
        Distance per update step in units of the interaction radius.
        """
        return self.shift_prog['shift'].value

    @property
    def noise_amplitude(self):
        """
        Amplitude of the angular noise as a fraction of TWOPI.
        """
        return self.align_prog['noise'].value

    def render(self):

        self.in_texture.use(location=0)
        self.vao.render(gl.POINTS, vertices=self.in_texture.width)
        self.ctx.finish()

        elapsed = self.clock.elapsed()
        if self.paused:
            self.runtime = elapsed
            return

        # now transform: first SHIFT
        # return

        self.in_texture.use(location=0)
        self.tao.transform(self.out_buffer, vertices=self.in_texture.width)
        self.ctx.finish()
        self.out_texture.write(self.out_buffer)

        # SWAP THE TEXTURES
        self.in_texture, self.out_texture = self.out_texture, self.in_texture

        # now ALIGN
        self.align_prog['seed'] = np.random.random()
        self.in_texture.use(location=0)
        self.tao2.transform(self.out_buffer, vertices=self.in_texture.width)
        self.ctx.finish()
        self.out_texture.write(self.out_buffer)

        # SWAP THE TEXTURES AGAIN
        self.in_texture, self.out_texture = self.out_texture, self.in_texture

        self.frames += 1
        dt = elapsed - self.runtime
        if dt > 1e3:
            self.label1.setText(
                '<p style="color: gold;font-size: x-large;font-family:Courier New,fixed;font-weight:600;">'
                f'order param.: {self.order_parameter:.3f}')
            self.label2.setText(
                '<p style="color: gold;font-size: x-large;font-family:Courier New,fixed;font-weight:600;">'
                f'fps: {self.frames / dt * 1e3:.1f}')
            self.frames = 0
            self.runtime = self.clock.elapsed()

    def mouseReleaseEvent(self, event):

        self.paused = not self.paused


if __name__ == '__main__':

    from sys import argv, exit

    app = QApplication(argv)
    mw = GLVicsekWidget()
    mw.show()
    exit(app.exec_())
