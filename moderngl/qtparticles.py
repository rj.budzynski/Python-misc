#! /usr/bin/python3

from PyQt5.QtWidgets import (QApplication, QCheckBox, QDockWidget, QGridLayout,
                             QGroupBox, QLabel, QMainWindow, QOpenGLWidget,
                             QPushButton, QSlider, QVBoxLayout, QWidget)
from PyQt5.QtGui import (QBrush, QColor, QFont, QGradient, QSurfaceFormat,
                         QTransform)
from PyQt5.Qt import QElapsedTimer, QTimer
from PyQt5.QtCore import QStandardPaths, Qt, pyqtSignal
import moderngl as gl
import pyqtgraph as pg
import numpy as np
from random import random
import numba

pg.setConfigOptions(background='#1d1e25ff',
                    foreground='#ffffff60',
                    antialias=True)

TWOPI = np.pi * 2

FALLBACKS = {
    # physics settings
    'N': 15000,
    'W': 180,
    'H': 180,
    'speed': .25,
    'noise': .25,
    'interacting': True,

    # visual settings
    'point_radius': .5,
    'window_height': 680,
    'paintTrails': True,
    'fadeAlpha': .04,
    # throttle rate of updates (NOT fps): delay in ms
    'update_interval': 15,
    'stats_update_interval': 1000  # ms
}


class Window(QMainWindow):
    def __init__(self, **kwargs):
        ...
        super().__init__()
        self.canvas = MyWidget(**kwargs)
        self.setCentralWidget(self.canvas)

        self.dockRight = QDockWidget('parameters', self)
        self.dockRight.setAllowedAreas(Qt.RightDockWidgetArea)
        self.dockRight.setFeatures(QDockWidget.NoDockWidgetFeatures)
        self.dockRight.setMinimumWidth(200)
        self.dockRight.setMaximumWidth(600)
        self.addDockWidget(Qt.RightDockWidgetArea, self.dockRight)
        self.controlPanel = p = ControlPanel()
        self.updateControlPanel()
        p.widthSlider.valueChanged.connect(
            lambda W: self.resetCentralWidget(W=W, H=W))
        p.numberSlider.valueChanged.connect(
            lambda N: self.resetCentralWidget(N=N))
        p.quitButton.clicked.connect(self.close)
        self.dockRight.setWidget(self.controlPanel)

        self.dockLeft = QDockWidget('Vicsek model')
        self.dockLeft.setAllowedAreas(Qt.LeftDockWidgetArea)
        self.dockLeft.setFeatures(QDockWidget.NoDockWidgetFeatures)
        self.dockLeft.setMinimumWidth(100)
        self.dockLeft.setMaximumWidth(400)
        self.addDockWidget(Qt.LeftDockWidgetArea, self.dockLeft)
        self.chartWidget = ChartWidget()
        self.dockLeft.setWidget(self.chartWidget)

        self.setWindowTitle(
            f'{self.canvas.N} Particles Moving Somewhat Randomly')
        self.connectSignalsToCanvas()
        self.show()

    def connectSignalsToCanvas(self):
        ...
        c, p, ch = self.canvas, self.controlPanel, self.chartWidget
        p.noiseSlider.valueChanged.connect(lambda v: c.setNoise(v / 100))
        p.speedSlider.valueChanged.connect(lambda v: c.setSpeed(v / 100))
        p.radiusSlider.valueChanged.connect(
            lambda r: c.setPointRadius(r / 100))
        p.randomizeButton.clicked.connect(c.randomize)
        p.randomizeButton.clicked.connect(ch.plot1.reset)
        p.randomizeButton.clicked.connect(ch.plot2.reset)
        p.resetButton.clicked.connect(
            lambda: self.resetCentralWidget(**FALLBACKS))
        p.interactionToggle.stateChanged.connect(
            lambda v: c.setInteracting(not v))
        p.trailsToggle.stateChanged.connect(lambda v: c.setPaintTrails(v))
        c.statsUpdate.connect(ch.updateStats)

    def resetCentralWidget(self, **kwargs):
        ...
        cfg = FALLBACKS.copy()
        for k in cfg:
            cfg[k] = getattr(self.canvas, k)
        cfg.update(kwargs or {})
        p = self.controlPanel
        p.noiseSlider.disconnect()
        p.speedSlider.disconnect()
        p.radiusSlider.disconnect()
        p.randomizeButton.disconnect()
        p.interactionToggle.disconnect()
        p.trailsToggle.disconnect()
        self.canvas.close()
        del self.canvas
        self.canvas = MyWidget(**cfg)
        self.setCentralWidget(self.canvas)
        self.updateControlPanel()
        self.connectSignalsToCanvas()
        self.chartWidget.plot1.reset()
        self.chartWidget.plot2.reset()

    def updateControlPanel(self):
        ...
        p, c = self.controlPanel, self.canvas
        p.connectSignals()
        p.widthSlider.setValue(c.W)
        p.numberSlider.setValue(c.N)
        p.noiseSlider.setValue(int(c.noise * 100))
        p.speedSlider.setValue(int(c.speed * 100))
        p.trailsToggle.setChecked(c.paintTrails)
        p.radiusSlider.setValue(int(c.point_radius * 100))
        p.interactionToggle.setChecked(not c.interacting)
        p.labels[1].setText(f'number of particles: {c.N}')
        p.labels[2].setText(f'box width: {c.W}')
        p.labels[5].setText(f'particle density: {c.N / c.W / c.H:.3f}')
        self.setWindowTitle(f'{c.N} Particles Moving Somewhat Randomly')

    def keyPressEvent(self, event):
        key = event.key()
        if key == Qt.Key_Q:
            self.close()
        else:
            super().keyPressEvent(event)


class ChartWidget(QWidget):
    def __init__(self):
        ...
        super().__init__()
        layout = QVBoxLayout()
        self.label = QLabel('<p style="color:gold;">ready ...<br>...')
        box1 = QGroupBox()
        box1.vbox = QVBoxLayout()
        box1.vbox.addWidget(self.label)
        self.plot1 = PlotWidget()
        self.plot1.setMinimumSize(200, 180)
        self.plot1.setTitle('<p style="font-size:small;font-weight:100;">'
                            'direction order vs. time')
        box1.vbox.addWidget(self.plot1)
        self.plot2 = PlotWidget()
        self.plot2.setMinimumSize(200, 180)
        self.plot2.setTitle('<p style="font-size:small;font-weight:100;">'
                            'clumping number vs. time')
        self.plot2.getViewBox().enableAutoRange(axis='y')
        self.plot2.getViewBox().setLimits(yMin=0)
        box1.vbox.addWidget(self.plot2)
        box1.setLayout(box1.vbox)
        layout.addWidget(box1, stretch=2)
        layout.addStretch(12)
        self.setLayout(layout)

    def updateStats(self, updates, order_parameter, clumping_index, hz):
        self.plot1.updatePlot(updates, order_parameter)
        self.plot2.updatePlot(updates, clumping_index)
        self.label.setText(
            '<p style="color:gold;">'
            f'{hz:.0f} Hz<br>'
            f'last {self.plot1.X[-1] - self.plot1.X[0]} updates of {updates}')
        self.plot1.setTitle(
            '<p style="font-size:small;color:gold;">order parameter: '
            f'{order_parameter:.3f}')
        self.plot2.setTitle(
            '<p style="font-size:small;color:gold;">clumping number: '
            f'{clumping_index:.2f}')


class PlotWidget(pg.PlotWidget):
    ...
    timerange = 4000

    def __init__(self):
        ...
        super().__init__()
        grad = QGradient(QGradient.PremiumDark)
        grad.setColorAt(0., QColor('#ff2e303a'))
        grad.setColorAt(1., QColor('#ff131319'))
        bgBrush = QBrush(grad)
        bgBrush.setTransform(QTransform().rotate(90., 2))
        self.hideButtons()
        self.setMouseEnabled(False, False)
        self.setBackground(bgBrush)
        # self.setTitle(
        #     '<p style="font-size:small;font-weight:100;">'
        #     'direction order vs. time'
        # )
        # self.getAxis('left').setStyle(showValues=False)
        self.getAxis('left').setTickFont(QFont(QFont().defaultFamily(), 7))
        self.getAxis('bottom').setStyle(showValues=False)
        self.clear()
        self.X, self.Y = [0], [0]
        self.plot(self.X, self.Y)
        self.plot([0], [0])
        self.showGrid(x=False, y=True)
        self.getViewBox().setRange(xRange=(0, self.timerange - 1),
                                   yRange=(0, 1),
                                   padding=0)
        self.dataItem, self.baseItem = self.listDataItems()
        self.addItem(
            pg.FillBetweenItem(self.dataItem,
                               self.baseItem,
                               brush=pg.mkBrush('#ffffff30')))

    def updatePlot(self, x, y):
        ...
        self.X.append(x)
        self.Y.append(y)
        while x - self.X[0] >= self.timerange:
            self.X.pop(0)
            self.Y.pop(0)
        x0 = self.X[0]
        t = [xx - x0 for xx in self.X]
        self.dataItem.setData(t, self.Y)
        self.baseItem.setData((t[0], t[-1]), (0, 0))

    def reset(self):
        ...
        self.X = [0]
        self.Y = [0]
        self.dataItem.setData(self.X, self.Y)
        self.baseItem.setData(self.X, self.Y)
        self.setTitle('<p style="font-size:small;color:gold;">ready ...')


class ControlPanel(QWidget):
    def __init__(self):
        ...
        super().__init__()
        self.layout = QVBoxLayout()
        self.box1 = QGroupBox()
        self.labels = [
            QLabel('<b>physical</b>'),
            QLabel('number of particles:'),
            QLabel('box width:'),
            QLabel('noise level:'),
            QLabel('speed:'),
            QLabel('particle density:')
        ]
        self.box1.vbox = QVBoxLayout()
        for label in self.labels:
            self.box1.vbox.addWidget(label)
        self.box1.vbox.insertSpacing(1, 10)
        self.numberSlider = QSlider(Qt.Horizontal)
        self.numberSlider.setMinimum(10)
        self.numberSlider.setMaximum(300000)
        self.numberSlider.setTracking(False)
        self.box1.vbox.insertWidget(2, self.numberSlider)
        self.widthSlider = QSlider(Qt.Horizontal)
        self.widthSlider.setMinimum(10)
        self.widthSlider.setMaximum(800)
        self.widthSlider.setTracking(False)
        self.box1.vbox.insertWidget(4, self.widthSlider)
        self.noiseSlider = QSlider(Qt.Horizontal)
        # noise slider values are percentages
        self.noiseSlider.setMinimum(0)
        self.noiseSlider.setMaximum(100)
        self.box1.vbox.insertWidget(6, self.noiseSlider)
        self.speedSlider = QSlider(Qt.Horizontal)
        # speed slider values are percentages
        self.speedSlider.setMinimum(1)
        self.speedSlider.setMaximum(100)
        self.box1.vbox.insertWidget(8, self.speedSlider)
        self.interactionToggle = QCheckBox('switch off interaction?')
        # self.interactionToggle.setChecked(False)
        self.box1.vbox.insertWidget(10, self.interactionToggle)
        self.box1.vbox.insertSpacing(11, 10)
        self.box1.setLayout(self.box1.vbox)
        self.layout.addWidget(self.box1)
        self.box2 = QGroupBox()
        moreLabels = [QLabel('<b>visual</b>'), QLabel('particle radius:')]
        self.labels.extend(moreLabels)
        self.box2.vbox = QVBoxLayout()
        for label in moreLabels:
            self.box2.vbox.addWidget(label)
        self.box2.vbox.insertSpacing(1, 10)
        self.radiusSlider = QSlider(Qt.Horizontal)
        # again percentages
        self.radiusSlider.setMinimum(5)
        self.radiusSlider.setMaximum(100)
        self.box2.vbox.insertWidget(2, self.radiusSlider)
        self.trailsToggle = QCheckBox('paint particle trails?')
        self.box2.vbox.insertWidget(2, self.trailsToggle)
        self.box2.setLayout(self.box2.vbox)
        self.layout.addWidget(self.box2)
        self.box3 = QGroupBox()
        self.box3.grid = QGridLayout()
        self.box3.setLayout(self.box3.grid)
        self.randomizeButton = QPushButton('randomize')
        self.box3.grid.addWidget(self.randomizeButton, 0, 0)
        self.quitButton = QPushButton('quit')
        self.box3.grid.addWidget(self.quitButton, 0, 1)
        self.resetButton = QPushButton('reset settings')
        self.box3.grid.addWidget(self.resetButton, 1, 0, 1, 2)
        self.layout.addWidget(self.box3)
        self.layout.addStretch()
        credits = QLabel(
            '<a href=https://rjb-vicsek.surge.sh/DESCRIPTION.md.html>description</a>'
            ' | <a href=https://gitlab.com/rj.budzynski/Python-misc/-/blob/master/moderngl/qtparticles.py>'
            'source code</a><p>'
            '&copy; 2021 RJ Budzyński<br>&lt;robert@budzynski.xyz&gt;')
        credits.setAlignment(Qt.AlignRight)
        credits.setOpenExternalLinks(True)
        self.layout.addWidget(credits)
        self.setLayout(self.layout)
        self.connectSignals()

    def connectSignals(self):
        ...
        self.numberSlider.sliderMoved.connect(lambda value: self.labels[
            1].setText(f'number of particles: {value}'))
        self.widthSlider.sliderMoved.connect(
            lambda value: self.labels[2].setText(f'box width: {value}'))
        self.noiseSlider.valueChanged.connect(
            lambda value: self.labels[3].setText(f'noise level: {value/100}'))
        self.speedSlider.valueChanged.connect(
            lambda value: self.labels[4].setText(f'speed: {value / 100}'))
        self.radiusSlider.valueChanged.connect(lambda value: self.labels[
            7].setText(f'particle radius: {value / 100}'))
        reportDensity = lambda number, width: self.labels[5].setText(
            f'particle density: {number / width / width:.3f}')
        self.widthSlider.sliderMoved.connect(
            lambda width: reportDensity(self.numberSlider.value(), width))
        self.numberSlider.sliderMoved.connect(
            lambda number: reportDensity(number, self.widthSlider.value()))


class MyWidget(QOpenGLWidget):
    ...
    statsUpdate = pyqtSignal(int, float, float, float)
    """
    Here's where the action is.
    """
    def __init__(self, **kwargs):
        ...
        super().__init__()

        myconfig = FALLBACKS.copy()
        myconfig.update(kwargs or {})
        for k, v in myconfig.items():
            self.__dict__[k] = v

        fmt = QSurfaceFormat()
        fmt.setVersion(3, 3)
        fmt.setProfile(QSurfaceFormat.CoreProfile)
        fmt.setSwapInterval(1)
        self.setFormat(fmt)
        self.ctx = None

        self.scale = self.window_height / self.H
        self.resize(int(self.window_height * self.W / self.H),
                    self.window_height)
        self.setMaximumSize(self.size())
        self.setMinimumSize(self.size())

        self.timer = QTimer()
        self.clock = QElapsedTimer()
        self.statsClock = QElapsedTimer()
        self.hz = None

        self._randomize = False

    def paintGL(self):
        ...
        self.setup()
        self.paintGL = self.render

    def setup(self):
        ...
        if self.ctx is None:
            self.ctx = gl.create_context()
            self.ctx.enable(gl.BLEND | gl.PROGRAM_POINT_SIZE)
            self.setUpdateBehavior(self.PartialUpdate)

            # PROGRAMS

            self.move_program = self.ctx.program(vertex_shader=move_vshader,
                                                 varyings=['v_data'])
            self.render_program = self.ctx.program(vertex_shader=vshader,
                                                   fragment_shader=fshader)
            self.align_program = self.ctx.program(vertex_shader=align_vshader,
                                                  varyings=['v_data'])
            self.fade_program = self.ctx.program(vertex_shader=blend_vshader,
                                                 fragment_shader=fade_fshader)

        # CONSTANT UNIFORMS

        self.move_program['dimensions'] = self.W, self.H
        self.move_program['speed'] = self.speed

        self.align_program['N'] = self.N
        self.align_program['noise'] = self.noise
        self.align_program['data'] = 0
        self.align_program['offsets'] = 1
        self.align_program['dimensions'] = self.W, self.H
        self.align_program['interacting'] = self.interacting

        self.render_program['dimensions'] = self.W, self.H
        self.render_program['pointSize'] = self.point_radius * self.scale * 2

        self.fade_program['fadeAlpha'] = self.fadeAlpha

        # STORAGE SETUP

        data_texture_width = min(8192, self.N)
        data_texture_height = self.N // 8192 + 1
        data_texture_size = data_texture_width, data_texture_height

        # initial conditions

        data = np.zeros((data_texture_width * data_texture_height, 4),
                        dtype='f4')
        data[:self.N, :3] = np.random.random((self.N, 3)) * np.array(
            (self.W, self.H, TWOPI))
        particle_texture0 = self.ctx.texture(data_texture_size,
                                             4,
                                             data.tobytes(),
                                             dtype='f4')
        particle_texture0.filter = gl.NEAREST, gl.NEAREST
        particle_texture1 = self.ctx.texture(data_texture_size, 4, dtype='f4')
        particle_texture1.filter = gl.NEAREST, gl.NEAREST
        self.particle_textures = [particle_texture0, particle_texture1]

        self.move_vao = self.ctx.vertex_array(self.move_program, [])
        self.align_vao = self.ctx.vertex_array(self.align_program, [])
        self.out_buffer = self.ctx.buffer(reserve=data.nbytes, dynamic=True)

        number_of_cells = self.W * self.H
        offsets_texture_width = min(8192, number_of_cells)
        offsets_texture_height = number_of_cells // 8192 + 1
        offsets_texture_size = offsets_texture_width, offsets_texture_height
        self.offsets_texture = self.ctx.texture(offsets_texture_size,
                                                1,
                                                dtype='u4')
        self.offsets_texture.filter = gl.NEAREST, gl.NEAREST

        self.render_vao = self.ctx.vertex_array(self.render_program, [])
        self.fade_vao = self.ctx.vertex_array(self.fade_program, [])

        # PAINT THE INITIAL DATA
        self.paint()

        # GUI RELATED

        self.timer.timeout.connect(self.update)
        self.mouseReleaseEvent = self.togglePaused
        self.setToolTip('click to start/pause')
        self.runtime = 0
        self.updates = 0
        self.totalUpdates = 0

    def paint(self):
        ...
        self.particle_textures[0].use(location=0)
        self.render_vao.render(gl.POINTS, self.N)

    def render(self):
        ...
        if self._randomize:
            tw, th = self.particle_textures[0].size
            data = np.zeros((tw * th, 4), dtype='f4')
            data[:self.N, :3] = np.random.random((self.N, 3)) * np.array(
                (self.W, self.H, TWOPI))
            self.particle_textures[0].write(data)
            self.ctx.clear()
            self.paint()
            self._randomize = False
            return

        # IF NOT IN PAUSED STATE, UPDATE DATA
        # throttle rate of updates for better visual

        if (self.clock.isValid()
                and self.clock.elapsed() > self.update_interval):

            # RENDER DATA IN TWO STAGES:

            # fade out previous state
            if self.paintTrails:
                self.fade_vao.render(gl.TRIANGLE_FAN, 4)
            else:
                self.ctx.clear()

            # - render current data
            self.paint()

            in_texture, out_texture = self.particle_textures

            # MOVE POSITIONS

            in_texture.use(location=0)
            self.move_vao.transform(self.out_buffer, vertices=self.N)

            self.data = np.frombuffer(self.out_buffer.read(self.N * 16),
                                      dtype='f4').reshape((self.N, 4))

            # SORT THE DATA BY CELL & COMPUTE TABLE OF OFFSETS

            data, offsets = counting_sort(self.data, self.W * self.H)
            data = np.resize(data, (out_texture.width * out_texture.height, 4))

            offsets = np.resize(
                offsets,
                (self.offsets_texture.width, self.offsets_texture.height))

            self.offsets_texture.write(offsets.tobytes())
            out_texture.write(data.tobytes())

            # PERFORM VELOCITY ALIGNMENT

            out_texture.use(location=0)
            self.offsets_texture.use(location=1)
            self.align_program['seed'] = random()
            self.align_vao.transform(self.out_buffer, vertices=self.N)
            in_texture.write(self.out_buffer)

            # GUI STUFF

            self.updates += 1
            self.totalUpdates += 1
            dt = self.statsClock.elapsed()
            if self.statsClock.isValid() and dt >= self.stats_update_interval:
                self.hz = self.updates / dt * 1e3
                self.statsUpdate.emit(self.totalUpdates, self.order_parameter,
                                      self.clumping_index, self.hz)
                self.updates = 0
                self.statsClock.restart()
            self.runtime += self.clock.restart()

    def togglePaused(self, event=None):
        ...
        if self.clock.isValid():
            self.timer.stop()
            self.clock.invalidate()
            self.statsClock.invalidate()
        else:
            self.timer.start()
            self.clock.start()
            self.statsClock.start()

    def randomize(self):
        ...
        self._randomize = True
        self.totalUpdates = 0
        self.update()

    @property
    def order_parameter(self):
        ...
        theta = self.data[:, 2]
        return np.hypot(np.cos(theta).mean(), np.sin(theta).mean())

    @property
    def clumping_index(self):
        """
        The average number of other particles within the interaction radius
        of a particle.
        """
        data = np.frombuffer(self.out_buffer.read(self.N * 16),
                             dtype='f4').reshape((self.N, 4))
        return data[:, 3].mean()

    def setPaintTrails(self, v):
        self.paintTrails = bool(v)

    def setPointRadius(self, radius):
        self.point_radius = radius
        self.render_program['pointSize'] = self.point_radius * self.scale * 2

    def setSpeed(self, v):
        self.speed = v
        self.move_program['speed'] = self.speed

    def setNoise(self, v):
        self.noise = v
        self.align_program['noise'] = self.noise

    def setInteracting(self, v):
        self.interacting = bool(v)
        self.align_program['interacting'] = self.interacting


@numba.jit
def generate_offsets(data, N, Ncells):
    offsets = np.empty(Ncells, dtype='uint32')
    offsets.fill(0xffffffff)
    prev_cell = 0xffffffff
    for i, cell in enumerate(data[:N, 3]):
        icell = int(cell)
        if icell != prev_cell:
            offsets[icell] = i
            prev_cell = icell
    return offsets


@numba.jit
def counting_sort(data, Ncells):
    offsets = np.zeros((Ncells, ), dtype='uint32')
    cellnumbers = data[:, 3]
    for cell in cellnumbers:
        offsets[int(cell)] += 1
    cum = 0
    for i, n in enumerate(offsets):
        if not n:
            offsets[i] = 0xffffffff
        else:
            offsets[i], cum = cum, cum + offsets[i]
    insert_positions, tmp_data = np.copy(offsets), np.empty_like(data)
    for vec in data:
        cell = int(vec[3])
        i = insert_positions[cell]
        tmp_data[i] = vec
        insert_positions[cell] += 1
    return tmp_data, offsets


move_vshader = """#version 330
#define TWOPI radians(360.)
uniform sampler2D data;
uniform vec2 dimensions;
uniform float speed;
out vec4 v_data;

ivec2 ind(int k) {
    return ivec2(k % 8192, k / 8192);
}

void main(){
    vec3 in_vert = texelFetch(data, ind(gl_VertexID), 0).xyz;
    vec2 pos = in_vert.xy;
    float theta = in_vert.z;
    pos += speed * vec2(cos(theta), sin(theta));
    pos = fract((pos + dimensions) / dimensions) * dimensions;
    float cell = floor(pos.x) + dimensions.y * floor(pos.y);
    v_data = vec4(pos, theta, cell);
}
"""

align_vshader = """#version 330
#define TWOPI radians(360.)
uniform int N;
uniform sampler2D data;
uniform usampler2D offsets;
uniform vec2 dimensions;
uniform float noise;
uniform float seed;
uniform bool interacting;
out vec4 v_data;

#define MANTISSA_MASK 0x007FFFFFu
#define FLOAT_ONE     0x3F800000u

uint hash(uvec2 xy) {
    uint x = xy.x;
    uint y = xy.y;
    x += x >> 11;
    x ^= x << 7;
    x += y;
    x ^= x << 6;
    x += x >> 15;
    x ^= x << 5;
    x += x >> 12;
    x ^= x << 9;
    return x;
}

float random(vec2 uv) {
    uvec2 bits = floatBitsToUint(uv);
    uint h = hash(bits);
    h &= MANTISSA_MASK;
    h |= FLOAT_ONE;

    float r2 = uintBitsToFloat(h);
    return r2 - 1.;
}

ivec2 ind(int k) {
    return ivec2(k % 8192, k / 8192);
}

void main() {
    int W = int(dimensions.x), H = int(dimensions.y);
    ivec2 dataSize = textureSize(data, 0);
    vec4 in_vert = texelFetch(data, ind(gl_VertexID), 0);
    vec2 pos = in_vert.xy;
    float theta = in_vert.z;
    int n = 0;

    // THE ACTUAL ALIGNMENT HAPPENS HERE
    int thisCell = int(in_vert.w);
    int[9] neighborCells = int[](thisCell, thisCell+1, thisCell+W+1,
        thisCell+W, thisCell+W-1, thisCell-1, thisCell-W-1, thisCell-W,
        thisCell-W-1);
    float s = 0, c = 0;
    //int n = 0;
    for(int i = 0; i < 9; ++i) {
        float x = pos.x, y = pos.y;
        int cell = neighborCells[i];
        int cellX = cell % W, cellY = cell / W;
        if (cellX < 0){ cellX = W - 1; x += W; }
        if (cellX == W){ cellX = 0; x -= W; }
        if (cellY < 0){ cellY = H - 1; y += H; }
        if (cellY == H){ cellY = 0; y -= H; }
        int offset = int(texelFetch(offsets, ind(cell), 0).r);
        while (offset < N){
            vec4 vert = texelFetch(data, ind(offset), 0);
            if (int(vert.w) != cell) break;
            if (distance(vert.xy, vec2(x, y)) < 1.){
                ++n;
                if (interacting) {
                    float th = vert.z;
#ifdef NEMATIC
                    float angle = theta - th;
                    float signFactor = sign(cos(angle));
                    s += signFactor * sin(th);
                    c += signFactor * cos(th);
#else
                    s += sin(th);
                    c += cos(th);
#endif
                }
            }
            ++offset;
        }
    }
    if (interacting) {
        s /= n;
        c /= n;

        theta = atan(s, c);
    // END OF ALIGNMENT PROCEDURE
    }
    theta += (random(seed * pos) - .5) * TWOPI * noise;
    
    // store the number of interacting particles as v_data.w
    v_data = vec4(pos, theta, float(--n));
}
"""

vshader = """#version 330
#define TWOPI radians(360.)
uniform sampler2D data;
uniform float N;
uniform vec2 dimensions;
uniform float pointSize;
out vec4 v_color;

vec3 hsv_to_rgb(vec3 hsv) {
    float v = hsv.z;
    if (hsv.y == 0.) return vec3(v, v, v);
    float f = fract(6. * hsv.x);
    float p = v * (1. - hsv.y);
    float q = v * (1. - f * hsv.y);
    float t = v * (1. - hsv.y * (1. - f));
    int i = int(floor(6. * hsv.x)) % 6; 
    switch (i) {
        case 0: return vec3(v, t, p);
        case 1: return vec3(q, v, p);
        case 2: return vec3(p, v, t);
        case 3: return vec3(p, q, v);
        case 4: return vec3(t, p, v);
        case 5: return vec3(v, p, q);
    }
}

ivec2 ind(int k) {
    return ivec2(k % 8192, k / 8192);
}

void main(){
    vec3 in_vert = texelFetch(data, ind(gl_VertexID), 0).xyz;
    vec2 pos = in_vert.xy / dimensions;
    float theta = in_vert.z;
    float hue = fract((theta < 0. ? theta + TWOPI : theta) / TWOPI);
    pos = 2. * pos - 1.;
    gl_Position = vec4(pos, 0., 1.);
    gl_PointSize = pointSize;
    v_color = vec4(hsv_to_rgb(vec3(hue, 1., 1.)), theta);
}
"""

fshader = """#version 330
in vec4 v_color;
out vec4 frag_color;

void main() {
    float theta = v_color.a;
    float c = cos(theta), s = sin(theta);
    float x = c*(2.*gl_PointCoord.x - 1.) - s*(2.*gl_PointCoord.y - 1.);
    float y = s*(2.*gl_PointCoord.x - 1.) + c*(2.*gl_PointCoord.y - 1.);
    frag_color = vec4(v_color.rgb, 1.)
          * clamp(0., 1., (
            step(0., -y - .25*x)
          * step(0., -.25*x + y)
          * smoothstep(-1., -.5, x)
          + step(-.02, -x*x-y*y)
            )
          )
//          + step(-1., -x*x-y*y)*step(.94, x*x+y*y) * vec4(1., 1., 1., .4)
        ;
}
"""

blend_vshader = """#version 330
vec2[4] vertices = vec2[](
    vec2(1., 1.), vec2(-1., 1.), vec2(-1., -1.), vec2(1., -1.)
);

void main(){
    gl_Position = vec4(vertices[gl_VertexID], 0., 1.);
}
"""

fade_fshader = """#version 330
precision highp float;
uniform float fadeAlpha;
out vec4 fragColor;

void main() {
    fragColor = vec4(0., 0., 0., fadeAlpha);
}"""

if __name__ == '__main__':

    from sys import argv, exit
    import json
    from os.path import abspath, dirname, join, exists
    from os import mkdir

    path = QStandardPaths.writableLocation(QStandardPaths.AppDataLocation)
    if path:
        path = join(path, 'vicsek')
        if not exists(path):
            try:
                mkdir(path)
            except OSError:
                path = ''
    if not path:
        path = dirname(abspath(__file__))

    confpath = join(path, 'vicsek.json')
    cfg = dict(FALLBACKS)
    try:
        with open(confpath) as conffile:
            cfg.update(json.load(conffile))
    except (FileNotFoundError, json.JSONDecodeError):
        pass

    def saveConfig():
        cfg = {}
        c = window.canvas
        for key in FALLBACKS:
            if getattr(c, key) != FALLBACKS[key]:
                cfg[key] = getattr(c, key)
        with open(confpath, 'w') as conffile:
            json.dump(cfg, conffile, indent=2)

    app = QApplication(argv)
    app.lastWindowClosed.connect(saveConfig)
    window = Window(**cfg)
    exit(app.exec_())
