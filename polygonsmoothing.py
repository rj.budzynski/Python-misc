#! /usr/bin/python3
'''See https://doi.org/10.1137/090746707
'''

from PyQt5.QtCore import QTimer
from PyQt5.Qt import QIntValidator
from PyQt5.QtWidgets import QMainWindow, QApplication, QDialog, QLineEdit

import pyqtgraph as pg
import numpy as np

pg.setConfigOption('background', '#2a2b35')
pg.setConfigOption('foreground', 0.98)


class PolygonSmoothing(pg.PlotWidget):

    visibleAxes = True
    steps = 0

    @property
    def running(self):
        return self.timer.isActive()

    def __init__(self):

        pg.PlotWidget.__init__(self)
        self.timer = QTimer()

    def setup(self, nVertices=50, interval=200):

        self.setAspectLocked(True)
        self.setTitle('Polygon Smoothing : <b>stopped</b> :'
                      'hit Space to start')

        self.points = np.zeros((nVertices + 1, 2))
        self.points[:-1] = np.random.random((nVertices, 2))
        self.points[-1] = self.points[0]
        self.plot(self.points, symbol='o')
        self.interval = interval
        self.timer.timeout.connect(self.update)
        self.toggleAxes()
        # self.disableAutoRange()
        # annotation = pg.TextItem(f'{self.steps} steps', anchor=(1, 1))
        # self.addItem(annotation)
        # self.text.setPos(0.5, 0.5)
        # self.text.setZValue(1777777)
        # print(self.getPlotItem().items)

    # def resizeEvent(self, event):
    #     size = self.size()
    #     # print(size)
    #     pg.PlotWidget.resizeEvent(self, event)
        # self.getPlotItem().items[-1].setPos(size.width(), size.height())

    def update(self):

        for _ in 1, 2:
            self.points[:-1] = (self.points[:-1] + self.points[1:]) / 2
            self.points[-1] = self.points[0]
        self.listDataItems()[0].setData(self.points)
        self.steps += 2
        # self.annotation.setText(f'{self.steps} steps')

    def startTimer(self):
        self.timer.start(self.interval)

    def stopTimer(self):
        self.timer.stop()

    def toggleAxes(self):

        if self.visibleAxes:
            for ax in 'left', 'bottom':
                self.hideAxis(ax)
        else:
            for ax in 'left', 'bottom':
                self.showAxis(ax)
        self.visibleAxes = not self.visibleAxes

    def keyPressEvent(self, event):
        text = event.text()
        if text == 'f':
            if not self.running:
                self.update()
        elif text == ' ':
            if self.running:
                self.timer.stop()
                self.setTitle(f"Polygon Smoothing : "
                              "<b>stopped</b> : "
                              "hit Space to restart")
            else:
                self.timer.start(self.interval)
                self.setTitle(f"Polygon Smoothing : "
                              "<b>running</b> : "
                              "hit Space to pause")
        elif text == '+':
            self.stopTimer()
            self.interval /= 1.2
            print(f'{1000 / self.interval} fps')
            self.startTimer()
        elif text == '-':
            self.stopTimer()
            self.interval *= 1.2
            print(f'{1000 / self.interval} fps')
            self.startTimer()
        elif text == 'x':
            self.toggleAxes()
        elif text == 'c':
            nVertices = len(self.points) - 1
            self.points[:-1] = np.random.random((nVertices, 2))
            self.points[-1] = self.points[0]
            self.listDataItems()[0].setData(self.points)
            self.steps = 0
            self.stopTimer()
            # self.text.setText(f'{self.steps} steps')
        # elif text == 'p':
        #     self.text.setText(f'{self.steps} steps')
        #     self.addItem(self.text)
        #     range = self.getPlotItem().getViewBox().viewRange()
        #     self.text.setPos(range[0][0], range[1][0])

        else:
            event.ignore()


# TODO
class SetupWidget(QDialog):

    def __init__(self, **kwargs):

        super(SetupWidget, self).__init__(**kwargs)
        self.kInp = QLineEdit()
        self.kInp.setMaxLength(3)
        self.kInp.setValidator(QIntValidator(1, 999))


if __name__ == '__main__':
    from sys import argv, exit

    app = QApplication(argv)

    try:
        nVertices = int(argv[1])
    except ValueError:
        exit(f'{argv[0]}: argument should be number of vertices (int)')
    except IndexError:
        nVertices = 13

    polySmooth = PolygonSmoothing()
    polySmooth.setup(nVertices=nVertices)
    mw = QMainWindow()
    mw.setGeometry(100, 100, 800, 800)
    mw.setWindowTitle('Polygon Smoothing via PyQtGraph')
    mw.keyPressEvent = (
        lambda ev: app.quit() if ev.text() == 'q' else ev.ignore())
    mw.setCentralWidget(polySmooth)
    mw.setContentsMargins(5, 5, 5, 5)
    mw.statusBar().showMessage("Press 'q' to quit; Space to run/pause; "
                               "'f' to single-step (when paused); "
                               "'x' to toggle axes; "
                               "'c' to clear and restart")
    mw.show()
    exit(app.exec())
