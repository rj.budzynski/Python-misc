#! /usr/bin/env python3

from numpy import array, sqrt, zeros, eye


class NewtonianDynamics(object):

    masses = array([1., 1., 1.])

    def __init__(self, nparticles=3, dim=2):
        '''Describe particles in `dim' dimensions (default: dim=2).
        '''
        self.nparticles = nparticles
        self.dim = dim

    def setMasses(self, masses):

        assert masses.shape == (self.nparticles,)
        assert all(masses > 0)

        self.masses = masses

    def leapfrog(self, X, V, δ=.01):

        assert X.shape == (self.nparticles, self.dim)
        assert V.shape == (self.nparticles, self.dim)

        acc = self.accel(X)

        while True:
            X += V * δ + acc * δ * δ / 2
            V += acc * δ / 2
            acc = self.accel(X)
            V += acc * δ / 2
            yield X, V

    def forceBackgroundCoulomb(self, X, g=1.):

        '''Sample force: central Coulomb attraction for mutually
        noninteracting particles.
        '''

        assert X.shape == (self.nparticles, self.dim)

        R = sqrt((X * X).sum(axis=1))
        f = [x / R**3 for k, x in enumerate(X)]
        return -g * array(f)

    def forceTwobodyCoulomb(self, X, g=1.):

        assert X.shape == (self.nparticles, self.dim)

        d, n = self.dim, self.nparticles
        R = zeros((n, n))
        for i in range(n):
            for k in range(i + 1, n):
                R[i, k] = sqrt(((X[i] - X[k])**2).sum())
        R += R.T + eye(n)
        R_3 = 1 / R**3 - eye(n)
        F = zeros((n, d))
        for k in range(n):
            F[k] = sum(self.masses[i] * (X[k] - X[i]) * R_3[i, k]
                       for i in range(n) if i != k)

        return -g * F

    def forceTwobodyCoulomb2(self, X, g=1.):

        d, n = self.dim, self.nparticles
        XX, R_3, f, F = (zeros((n, n, d)), zeros((n, n)),
                         zeros((n, n, d)), zeros((n, d)))
        for i in range(n - 1):
            for k in range(i + 1, n):
                XX[i, k] = X[i] - X[k]
                R_3[i, k] = sqrt((XX[i, k]**2).sum())**-3
                f[i, k] = XX[i, k] * R_3[i, k]
        for k in range(n):
            F[k] = sum(self.masses[i] * f[i, k] if i < k
                       else -f[k, i] * self.masses[i] for i in range(n))

        return F * g

    def energy(self, X, V, g=1.):

        ekin = ((V * V).sum(axis=1) * self.masses).sum() / 2
        n = self.nparticles

        R = zeros((n, n))

        for i in range(n):
            for k in range(i + 1, n):
                R[i, k] = sqrt(((X[i] - X[k])**2).sum())
        epot = -g * sum(self.masses[i] *
                        sum(
                            self.masses[k] / R[i, k]
                            for k in range(i + 1, n))
                        for i in range(n))

        return ekin + epot

    accel = forceTwobodyCoulomb2
    step = leapfrog
