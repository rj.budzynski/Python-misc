#! /usr/bin/python3

import numpy as np
from numpy.random import rand
import matplotlib
# Make sure that we are using QT5
matplotlib.use('Qt5Agg')
from matplotlib import interactive
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure as MplFigure
from matplotlib.animation import FuncAnimation
from matplotlib.text import Text
from PyQt5.QtWidgets import QMainWindow, QApplication, QSizePolicy
from sys import argv, exit


class BrownGas(object):
    def __init__(self, **kwargs):

        self.dim, self.npart = 2, 999
        if 'dim' in kwargs:
            self.dim = kwargs['dim']
        if 'npart' in kwargs:
            self.npart = kwargs['npart']
        self.positions = np.zeros((self.dim, self.npart))
        # self.positions = .5 * rand(self.dim, self.npart) - .25

    def step(self):

        r = rand(self.dim, self.npart) - .5
        self.positions += r / np.sqrt((r * r).sum(axis=0))


class MplGui(FigureCanvas):
    _running = False
    ticks = 0
    
    def __init__(self, parent=None, **kwargs):

        mfig = MplFigure(figsize=(8, 6), dpi=100, facecolor='#2a2b35')
        self.axes = mfig.add_subplot(111)
        self.axes.set_frame_on(True)
        self.axes.set_fc('#404050')
        self.axes.tick_params(colors='w')
        self.axes.get_yaxis().set_visible(False)
        # self.axes.set_position((.01, .1, .98, .90))
        self.axes.set_xlim(-100., 100.)
        self.axes.set_ylim(-100., 100.)
        self.brownGas = BrownGas(**kwargs)
        FigureCanvas.__init__(self, mfig)
        self.setParent(parent)
        FigureCanvas.setSizePolicy(self, QSizePolicy.Expanding,
                                   QSizePolicy.Expanding)
        FigureCanvas.updateGeometry(self)

    def setup(self, interval=200):

        # interactive(True)
        self.interval = interval
        axs = self.axes
        self._title = axs.set_title(
            f"A Gas of Brownian Particles : "
            # "stopped : "
            "hit Space to run / pause",
            color='w')
        axs.set_aspect(1)
        axs.grid(False)
        axs.axis('on')
        self.figure.text(
            .98, .02,
            '© 2019 Robert J Budzyński',
            color='.7',
            ha='right',
            figure=self.figure
        )
        self.figure.text(
            .02, .98,
            f'dimensions: {self.brownGas.dim}\nparticles: {self.brownGas.npart}\ntime interval: {self.interval} ms',
            color='.7',
            ha='left',
            va='top',
            figure=self.figure
        )
        self.elapsedLabel = self.figure.text(
            .02, .02,
            f'elapsed time: {self.ticks * interval / 1000:.1f} s',
            color='.7'
        )
        # in case of higher dimensionality project on 2d
        self.balls, = axs.plot(
                    *self.brownGas.positions[:2], 
                    linestyle='None', 
                    marker='o',
                    markerfacecolor='r',
                    markeredgecolor='r',
                    markersize=1.5
                )
        self._anim = FuncAnimation(
            self.figure,
            self.update_anim,
            init_func=self._init_anim,
            # blit=True,
            interval=self.interval
            )

    def _init_anim(self):

        return [self.balls]

    def update_anim(self, ticks):

        if not ticks:
            self._anim.event_source.stop()
        if ticks % 10 == 9:
            self.elapsedLabel.set_text(
                f'elapsed time: {ticks * interval / 1000:.1f} s'
                + f'\naverage distance: {np.sqrt((self.brownGas.positions**2).sum(axis=0)).mean():.1f}'
            )
        else:
            self.ticks = ticks
            self.brownGas.step()
            self.balls.set_data(self.brownGas.positions[:2])
            # print(self.balls.get_xydata())
        return [self.balls, self.elapsedLabel]

    def _stop(self):
        if self._running:
            self._anim.event_source.stop()
            self._running = False

    def _start(self):
        if not self._running:
            self._anim.event_source.start()
            self._running = True

    def handle_key(self, event):
        text = event.text()
        # print(f'key event: {text}')
        if text == ' ':
            if self._running:
                # This doesn't work for some reason:
                # self._title.set_text(
                #         f"A Gas of Brownian Particles : "
                #         "stopped : "
                #         "hit Space to resume"
                # )
                self._stop()
            else:
                # This doesn't work for some reason:
                # self._title.set_text(
                #     f"A Gas of Brownian Particles : "
                #     "running : "
                #     "hit Space to pause"
                # )
                self._start()
        # elif text == 'r':
        #     self.axes.relim()
        elif text == 'q':
            app.quit()
        else:
            pass

if __name__ == "__main__":

    dim, npart, interval = 2, 999, 200
    args = argv[0:1]
    for a in argv[1:]:
        if a.startswith('dim='):
            dim = int(a.split('=')[-1])
        elif a.startswith('npart='):
            npart = int(a.split('=')[-1])
        elif a.startswith('interval='):
            interval = int(a.split('=')[-1])
        else:
            args.append(a)

    app = QApplication(args)
    gasGui = MplGui(dim=dim, npart=npart)
    mw = QMainWindow()
    mw.setGeometry(100, 100, 800, 600)
    mw.setWindowTitle('A Brownian Gas via Matplotlib')
    mw.keyPressEvent = gasGui.handle_key
    mw.setCentralWidget(gasGui)
    mw.setContentsMargins(5, 5, 5, 5)
    gasGui.setup(interval=interval)
    mw.statusBar().showMessage(
        "Press 'q' to quit; Space to run/pause; "
    )
    mw.show()
    
    exit(app.exec())


__all__ = ['BrownGas', 'MplGui']
